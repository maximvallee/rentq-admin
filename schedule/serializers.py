from rest_framework import serializers
from accounts.models import User
from crm.models import Staff, Unit, Manager, Instruction, Prospect
from schedule.models import Availability, Appointment
from rest_framework.serializers import SerializerMethodField
import _datetime
from datetime import timedelta, datetime
from schedule.methods import send_multi_emails, suffix

from django.template import Context
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from schedule.methods import appointment_context

from .tasks import send_rsvp_task, send_reminder_task
import os
from RentQ3 import settings




# from sendgrid import SendGridAPIClient
# from sendgrid.helpers.mail import Mail


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name']



class InstructionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Instruction
        fields = ['name', 'value']

class StaffSerializer(serializers.ModelSerializer):
    # name = serializers.StringRelatedField(source='user',read_only=True)
    user = UserSerializer()


    class Meta:
        model = Staff
        fields = ['user_id', 'user', 'profile_pic']


class FilteredAppointmentSerializer(serializers.ListSerializer):
        # import pdb; pdb.set_trace() 
    def to_representation(self, data):

        start_buffer = 0 ## How many hours from now do we start displaying appointments in the queue
        end_buffer = 72 ## How many hours from now do we stop displaying appointments in the queue
        server_adj = self.root.instance[0].manager.user.timezone ## Server is x hours ahead so we must offset based on manager's chosen timezone
        start_cutoff = datetime.now() + timedelta(hours=server_adj) + timedelta(hours=start_buffer)
        end_cutoff = datetime.now() + timedelta(hours=server_adj) + timedelta(hours=end_buffer)
        data = data.all().exclude(appointment_time__lt=start_cutoff).exclude(appointment_time__gt=end_cutoff).order_by('appointment_time')
        return super(FilteredAppointmentSerializer, self).to_representation(data)


class AppointmentSerializer(serializers.ModelSerializer):

    class Meta:
        list_serializer_class = FilteredAppointmentSerializer ## To filter a nested serializer, we must subclass a ListSerializer and overwrite the to_representation method (above).
        model = Appointment
        fields = ['id', 'appointment_time', 'staff', 'unit']


class AvailabilitySerializer(serializers.ModelSerializer):
    staff = StaffSerializer() 

    class Meta:
        model = Availability
        fields = ['id','start_time', 'end_time','staff']

class ManagerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Manager
        fields = ['company', 'logo', 'minimum_notice', 'interval', 'queue_spread', 'queue_threshold']


class UnitSerializer(serializers.ModelSerializer):
    availability = SerializerMethodField()
    manager = ManagerSerializer()
    instruction = InstructionSerializer(source='instruction_set', many=True)
    appointment = AppointmentSerializer(many=True, source='appointment_set')

    class Meta:
        model = Unit
        fields = ['id', 'unit_no', 'address', 'appointment_type', 'manager', 'instruction', 'availability', 'appointment']
    
    def get_availability(self, instance): ## instance returns current request from user (ie, unit id)
        server_adj = instance.manager.user.timezone ## Server is x hours ahead so we must offset based on manager's chosen timezone
        start_cutoff = datetime.now() + timedelta(hours=server_adj)
        queryset = instance.availability_set.exclude(end_time__lt=start_cutoff).order_by('start_time') ## availability_set is the reverse lookup (opposite side of M2M) (ie, unit's availability)
        return AvailabilitySerializer(queryset, many=True).data

class ProspectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Prospect
        fields = [  'id','first_name', 'last_name', 'email', 'phone', 'unit_size', 'lease_term', 'occupation']


class AppointmentSerializer(serializers.ModelSerializer):
    prospect = ProspectSerializer()

    class Meta:
        model = Appointment
        fields = ['appointment_time', 'unit', 'staff', 'prospect', 'appointment_type']

    def create(self, validated_data):

        ### 1) Create Prospect and Appointment objects
        prospect_data = validated_data.pop('prospect')
        prospect, created = Prospect.objects.update_or_create(email=prospect_data.pop('email'), defaults=prospect_data)
        appointment = Appointment.objects.create(prospect=prospect, **validated_data)

        ### 2) Prepare email templates
        context = appointment_context(appointment)

        prospect_email = [
            [prospect.email, ],
            'Confirmed! Viewing at {0} ({1})'.format(context['TIME'],context['SHORT_ADDRESS']), 
            'schedule/confirmation_email_prospect.txt',
            'schedule/confirmation_email_prospect.html',
        ]

        staff_email = [
            [appointment.staff.user.email, ],
            '{0} scheduled a viewing at {1} ({2})'.format(context['PROSPECT_NAME'], context['TIME'], context['SHORT_ADDRESS']),
            'schedule/confirmation_email_staff.txt',
            'schedule/confirmation_email_staff.html',
        ]

        # 3) Send confirmation emails
        templates_array = [prospect_email, staff_email]
        send_multi_emails(context, templates_array)

        # 4) Create new tasks
        send_reminder_task.delay(appointment.id)
        send_rsvp_task.delay(appointment.id)

        return appointment
    
    
