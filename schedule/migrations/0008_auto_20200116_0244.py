# Generated by Django 3.0 on 2020-01-16 07:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0010_prospect'),
        ('schedule', '0007_auto_20200116_0217'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='prospect',
            field=models.ForeignKey(default=3, on_delete=django.db.models.deletion.CASCADE, to='crm.Prospect'),
            preserve_default=False,
        ),
    ]
