from datetime import datetime, timedelta, date
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.urls import reverse, reverse_lazy
from django.utils.safestring import mark_safe
import calendar
from rest_framework import viewsets
from django.db.models import Prefetch
from rest_framework.response import Response
from rest_framework.decorators import action
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, IsAdminUser
from schedule.methods import send_multi_emails, suffix

from accounts.models import User
from crm.models import Staff, Unit, Manager, Prospect
from schedule.models import Availability, Appointment
from schedule.forms import NewAvailabilityForm, UpdateAvailabilityForm, UpdateAppointmentForm
from schedule.utils import Calendar
from schedule.models import Availability
from schedule.serializers import AvailabilitySerializer, StaffSerializer, UnitSerializer, AppointmentSerializer

from django.template import Context
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from schedule.methods import send_multi_emails, send_multi_sms, appointment_context
from .tasks import send_rsvp_task, send_reminder_task

from RentQ3 import settings

from ipware import get_client_ip


# Create your views here.
class CalendarView(generic.TemplateView):
    model = Availability
    template_name = 'schedule/calendar.html'

    def get_context_data(self, **kwargs):
        # import pdb; pdb.set_trace()
        context = super().get_context_data(**kwargs)

        # user = self.request.user
        # kwargs['availabilities'] = Availability.objects.filter(staff__manager=Manager.objects.get(user=user))

        d = get_date(self.request.GET.get('month', None))
        cal = Calendar(d.year, d.month)
        html_cal = cal.formatmonth(withyear=True)
        context['calendar'] = mark_safe(html_cal)
        context['prev_month'] = prev_month(d)
        context['next_month'] = next_month(d)
        return context

def get_date(req_day):
    # import pdb; pdb.set_trace()
    if req_day:
        year, month = (int(x) for x in req_day.split('-'))
        return date(year, month, day=1)
    return datetime.today()

def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - timedelta(days=1)
    month = 'month=' + str(prev_month.year) + '-' + str(prev_month.month)
    return month

def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + timedelta(days=1)
    month = 'month=' + str(next_month.year) + '-' + str(next_month.month)
    return month

class AvailabilityCreateView(generic.CreateView):
    model = Availability
    template_name = 'schedule/availability_form.html'
    form_class = NewAvailabilityForm


    ## Add a kwarg 'user' (active user) to the form's kwargs. This user (ie, manager)
    ## is used to filter and display only his agents in the MultiSelect
    def get_form_kwargs(self):
        kwargs = super(AvailabilityCreateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, **kwargs):
        if self.request.method == 'GET':
            # import pdb; pdb.set_trace()
            selected_date_str = self.request.GET.get('date', None)
            selected_date = datetime.strptime(selected_date_str, '%Y-%m-%d')
            kwargs['selected_date'] = selected_date.strftime("%a, %d %b %Y")
        else:
            kwargs['selected_date'] = datetime.strptime(self.request.POST.get('selected_date'), '%Y-%m-%d')

        return super().get_context_data(**kwargs)

    def post(self, request, **kwargs):
        # import pdb; pdb.set_trace()
        selected_date_str = self.request.GET.get('date', None)
        selected_date = datetime.strptime(selected_date_str, '%Y-%m-%d')
        request.POST = request.POST.copy()
        request.POST['selected_date'] = selected_date
        return super(AvailabilityCreateView, self).post(request, **kwargs)


class AvailabilityUpdateView(generic.UpdateView):
    model = Availability
    template_name = 'schedule/availability_form.html'
    form_class = UpdateAvailabilityForm

    ## Add a kwarg 'user' (active user) to the form's kwargs. This user (ie, manager)
    ## is used to filter and display only his agents in the MultiSelect
    def get_form_kwargs(self):
        kwargs = super(AvailabilityUpdateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class AvailabilityDeleteView(generic.DeleteView):
    model = Availability
    template_name = 'schedule/availability_confirm_delete.html'
    success_url = reverse_lazy('schedule:calendar')



#### APPOINTMENTS ######


class AppointmentListView(generic.TemplateView):

    

    template_name = 'crm/appointment_list.html'

    def get_context_data(self, **kwargs):

        # client_ip, is_routable = get_client_ip(self.request)
        # print('**********************************')
        # print(client_ip)
        # print(is_routable)
        # import pdb; pdb.set_trace()  
        user = self.request.user
        now = datetime.now() + timedelta(hours=user.timezone)

        if user.is_manager:
            kwargs['appointments']  = Appointment.objects.filter(appointment_time__gt=now).filter(staff__manager=Manager.objects.get(user=user)).order_by('appointment_time')
        else:
            kwargs['appointments']  = Appointment.objects.filter(appointment_time__gt=now).filter(staff__user=Staff.objects.get(user=user)).order_by('appointment_time')
        return super().get_context_data(**kwargs)

class AppointmentUpdateView(generic.UpdateView):
    # import pdb; pdb.set_trace()

    model = Appointment
    template_name = 'schedule/appointment_update_form.html'
    form_class = UpdateAppointmentForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['appointment'] = self.object
        return context

    def form_valid(self, form):
        
        reason="Your viewing has been assigned to another agent."
        prev_appointment = Appointment.objects.get(id=self.object.id)
        new_appointment = self.object
        prev_context = appointment_context(prev_appointment, reason=reason)
        new_context = appointment_context(new_appointment, prev_date=prev_context['TIME'])
        prev_datetime = prev_appointment.appointment_time
        new_datetime = new_appointment.appointment_time
        prev_staff = prev_appointment.staff
        new_staff = new_appointment.staff
        is_new_datetime = prev_datetime.replace(microsecond=0) != new_datetime.replace(microsecond=0) ##  .replace() required to remove millisecond discrepency when Django stores in 64 bits
        is_new_staff = prev_staff != new_staff

        self.object = form.save()
   
        if is_new_datetime and is_new_staff:

            # 1) Prepare email templates
            prospect_email = [
                [new_appointment.prospect.email],
                'Update! Viewing has been rescheduled ({0})'.format(new_context['SHORT_ADDRESS']),
                'schedule/reschedule_email_prospect.txt',
                'schedule/reschedule_email_prospect.html'
            ]
            
            prev_staff_email = [
                [prev_staff.user.email],
                'Cancelled! Viewing {0} ({1})'.format(prev_context['TODAY_REFORMAT'], prev_context['SHORT_ADDRESS']),
                'schedule/cancellation_email_staff.txt',
                'schedule/cancellation_email_staff.html'
            ]

            new_staff_email = [
                [new_staff.user.email],
                '{0} scheduled a viewing {1} ({2})'.format(new_context['PROSPECT_NAME'], new_context['TODAY_REFORMAT'], new_context['SHORT_ADDRESS']),
                'schedule/confirmation_email_staff.txt',
                'schedule/confirmation_email_staff.html'
            ]

            # 2) Send update emails
            email_templates = [prospect_email]
            send_multi_emails(new_context, email_templates)

            email_templates = [prev_staff_email]
            send_multi_emails(prev_context, email_templates)

            email_templates = [new_staff_email]
            send_multi_emails(new_context, email_templates)

            # 3) Create new tasks
            send_reminder_task.delay(new_appointment.id)
            send_rsvp_task.delay(new_appointment.id)
            
        elif is_new_datetime:

            # 1) Prepare email templates
            prospect_email = [
                [new_appointment.prospect.email],
                'Update! Viewing has been rescheduled ({0})'.format(new_context['SHORT_ADDRESS']),
                'schedule/reschedule_email_prospect.txt',
                'schedule/reschedule_email_prospect.html'
            ]

            staff_email = [
                [new_appointment.staff.user.email],
                'Update! Viewing has been rescheduled ({0})'.format(new_context['SHORT_ADDRESS']),
                'schedule/reschedule_email_staff.txt',
                'schedule/reschedule_email_staff.html'
            ]

            # 2) Send update emails
            email_templates = [prospect_email, staff_email]
            send_multi_emails(new_context, email_templates)

            # 3) Create new tasks
            send_reminder_task.delay(new_appointment.id)
            send_rsvp_task.delay(new_appointment.id)

        elif is_new_staff:

            # 1) Prepare email templates
            prev_staff_email = [
                [prev_staff.user.email],
                'Cancelled! Viewing {0} ({1})'.format(prev_context['TODAY_REFORMAT'], prev_context['SHORT_ADDRESS']),
                'schedule/cancellation_email_staff.txt',
                'schedule/cancellation_email_staff.html'
            ]

            email_templates = [prev_staff_email]
            send_multi_emails(prev_context, email_templates)

            new_staff_email = [
                [new_staff.user.email],
                '{0} scheduled a viewing {1} ({2})'.format(new_context['PROSPECT_NAME'], new_context['TODAY_REFORMAT'], new_context['SHORT_ADDRESS']),
                'schedule/confirmation_email_staff.txt',
                'schedule/confirmation_email_staff.html'
            ]

            # 2) Send update emails
            email_templates = [new_staff_email]
            send_multi_emails(new_context, email_templates)

            # # 3) Create new tasks
            send_reminder_task.delay(new_appointment.id)
            send_rsvp_task.delay(new_appointment.id)

        return super().form_valid(form)


class AppointmentDeleteView(generic.DeleteView):
    # import pdb; pdb.set_trace()
    model = Appointment
    template_name = 'schedule/appointment_confirm_delete.html'
    success_url = reverse_lazy('schedule:appointment_list')
   
    def delete(self, request, *args, **kwargs):

        ## 1) Send email to staff and prospect
        self.object = self.get_object()
        success_url = self.get_success_url()
        appointment = self.get_object()
        reason = request.POST.get("reason")
        context = appointment_context(appointment, reason=reason)

        # 2) Prepare email templates
        prospect_email = [
            [appointment.prospect.email],
            'Cancelled! Viewing at {0} ({1})'.format(context['TIME'], context['SHORT_ADDRESS']),
            'schedule/cancellation_email_prospect.txt',
            'schedule/cancellation_email_prospect.html'
        ]

        staff_email = [
            [appointment.staff.user.email],
            'Cancelled! Viewing at {0} ({1})'.format(context['TIME'], context['SHORT_ADDRESS']),
            'schedule/cancellation_email_staff.txt',
            'schedule/cancellation_email_staff.html'
        ]

        # 3) Send cancellation emails
        email_templates = [prospect_email, staff_email]
        send_multi_emails(context, email_templates)

        ## 4) Delete object
        self.object.delete()

        if request.user.is_authenticated:
            return HttpResponseRedirect(success_url)
        else:
            return render(request,'schedule/cancellation_confirmation_page.html', context)

class AppointmentDeleteConfirmedView(generic.TemplateView):
    model = Appointment

def rsvp(request, pk):
    # import pdb; pdb.set_trace()

    ## 1) Update the appointment status to True
    Appointment.objects.filter(id=pk).update(status=True)
    appointment = Appointment.objects.get(id=pk)

    ## 2) Prepare SMS templates
    context = appointment_context(appointment)
    staff_sms = [
        [appointment.prospect.phone],
        'schedule/rsvp_confirmation_sms.txt'
    ]

    # 3) Send SMS confirmation
    sms_templates = [staff_sms]
    send_multi_sms(context, sms_templates)

    return render(request, 'schedule/rsvp_confirmation_prospect.html', context)

def send_reminder(request, pk):
    # import pdb; pdb.set_trace()

    ## 1) Get the instance
    appointment = Appointment.objects.get(id=pk)

    ## 2) Prepare notification templates
    context = appointment_context(appointment)
    prospect_email = [
        [appointment.prospect.email],
        'Reminder! Viewing {0} ({1})'.format(context['TODAY_REFORMAT'], context['SHORT_ADDRESS']),
        'schedule/reminder_email_prospect.txt',
        'schedule/reminder_email_prospect.html'
    ]
    staff_email = [
        [appointment.staff.user.email],
        'Reminder! Viewing {0} ({1})'.format(context['TODAY_REFORMAT'], context['SHORT_ADDRESS']),
        'schedule/reminder_email_staff.txt',
        'schedule/reminder_email_staff.html'
    ]
    templates_array = [prospect_email, staff_email]

    ## 3) Send notifications
    send_multi_emails(context, templates_array)

    return redirect('/calendar/appointments/')

def send_rsvp(request, pk):
    # import pdb; pdb.set_trace()

    ## 1) Get the instance
    appointment = Appointment.objects.get(id=pk)

    ## 2) Prepare notification templates
    ### Email
    context = appointment_context(appointment)
    prospect_email = [
        [appointment.prospect.email],
        'Action Required: Confirm your viewing {0} ({1})'.format(context['TODAY_REFORMAT'], context['SHORT_ADDRESS']),
        'schedule/rsvp_request_sms_prospect.txt',
        'schedule/rsvp_request_email_prospect.html'
    ]
    email_templates = [prospect_email]

    ### SMS
    prospect_sms = [
        [appointment.prospect.phone],
        'schedule/rsvp_request_sms_prospect.txt'
    ]
    sms_templates = [prospect_sms]
    
    ## 3) Send notifications
    send_multi_emails(context, email_templates)
    send_multi_sms(context, sms_templates)

    return redirect('/calendar/appointments/')


###### API ######

class UnitViewSet(viewsets.ModelViewSet):
    queryset = Unit.objects.all()
    serializer_class = UnitSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filterset_fields = ['id']
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly]

class AppointmentViewSet(viewsets.ModelViewSet):
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filterset_fields = ['id']
    authentication_classes = []
    permission_classes = []

