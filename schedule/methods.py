from accounts.models import User
from schedule.models import Availability, Appointment
from crm.models import Staff, Unit, Manager, Instruction, Prospect
import _datetime
from datetime import timedelta, datetime, timezone
import urllib.parse
from django.contrib.sites.models import Site

## EMAIL NOTIFICATION
from django.template import Context
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives

## SMS NOTIFICATION
from twilio.rest import Client
from RentQ3 import settings

# import pdb; pdb.set_trace()
def suffix(d):
    return 'th' if 11<=d<=13 else {1:'st',2:'nd',3:'rd'}.get(d%10, 'th')


def get_local_time(appointment):
    timezone = Manager.objects.filter(staff=appointment.staff.pk)[0].user.timezone
    now = datetime.now() + timedelta(hours=timezone) ## Client time (not server) + timedelta(minutes=-240)
    return now

def is_today_or_tomorrow(appointment):
    today = get_local_time(appointment)
    tomorrow = today + timedelta(1)
    appointment_time = appointment.appointment_time

    if appointment_time.date() == today.date():
        return 'today'
    elif appointment_time.date() == tomorrow.date():
        return 'tomorrow'
    else:
        return ''

def calculate_secs_until(appointment):
    # import pdb; pdb.set_trace()
    now = get_local_time(appointment)
    then = appointment.appointment_time.replace(tzinfo=None)
    secs_until = int((then - now).total_seconds())

    return secs_until

def appointment_context(appointment,**extra_parameters):
    manager = Manager.objects.filter(staff=appointment.staff.pk)[0]
    sfx = suffix(appointment.appointment_time.day)
   
    try:
        thumbnail = appointment.staff.profile_pic.url
    except:
        try:
            thumbnail = manager.logo.url
        except:
            thumbnail = "https://nxcso.stripocdn.email/content/guids/CABINET_2e4fd0cc2a6acaa7c872a55a322caa80/images/68011589068220664.png"

    context = {
        'COMPANY': manager.company,
        'PROSPECT_NAME': appointment.prospect.first_name,
        'PROSPECT_LAST': appointment.prospect.last_name,
        'PROSPECT_PHONE': appointment.prospect.phone,
        'PROSPECT_EMAIL': appointment.prospect.email,
        'OCCUPATION': appointment.prospect.occupation,
        'UNIT_SIZE': appointment.prospect.unit_size,
        'TERM': appointment.prospect.lease_term,
        'TYPE': appointment.appointment_type,
        'TIME': appointment.appointment_time.strftime("%I:%M %p on %a, %b %d") + sfx,
        'SHORT_TIME': appointment.appointment_time.strftime("%I:%M %p"),
        'ZOOM_URL':manager.zoom_meeting_room,
        'STAFF': User.objects.filter(staff=appointment.staff.user_id)[0].first_name,
        'STAFF_PHONE': appointment.staff.user.phone,
        'STAFF_EMAIL': appointment.staff.user.email, 
        'SHORT_ADDRESS': str(appointment.unit) + ", Unit " + str(appointment.unit.unit_no) if appointment.unit.unit_no else str(appointment.unit),
        'LONG_ADDRESS': urllib.parse.quote_plus(appointment.unit.address),
        'PK_APPOINTMENT': appointment.pk,
        'PK_UNIT': appointment.unit.pk,
        'THUMBNAIL': thumbnail,
        'URL_STARTTIME': appointment.appointment_time.strftime("%Y%m%d")+"T"+ appointment.appointment_time.strftime("%H%M%S"),
        'URL_ENDTIME': (appointment.appointment_time + timedelta(minutes=15)).strftime("%Y%m%d") + "T" + (appointment.appointment_time + timedelta(minutes=15)).strftime("%H%M%S"),
        'REASON': extra_parameters['reason'] if 'reason' in extra_parameters else '',
        'OLD_TIME': extra_parameters['prev_date'] if 'prev_date' in extra_parameters else '',
        'TIMER': int(calculate_secs_until(appointment)/60),
        'DEADLINE': 0 if int(calculate_secs_until(appointment)/60) - 45 < 0 else int(calculate_secs_until(appointment)/60) - 45,
        'DOMAIN' : Site.objects.get_current().domain,
        'PROTOCOL' : 'http',
        'IS_TODAY' : is_today_or_tomorrow(appointment),
        'TODAY_REFORMAT': 'at {0}'.format(appointment.appointment_time.strftime("%I:%M %p on %a, %b %d") + sfx) if is_today_or_tomorrow(appointment) == '' else '{0} at {1}'.format(is_today_or_tomorrow(appointment), appointment.appointment_time.strftime("%I:%M %p"))
        }
        
    return context


def send_multi_emails(context, email_templates):
    
    for j in email_templates:
        message = EmailMultiAlternatives(
            subject= j[1],
            body= get_template(j[2]).render(context),
            from_email= settings.DEFAULT_FROM_EMAIL,
            to= j[0],
            bcc= [settings.DEFAULT_FROM_EMAIL]
        )
        message.attach_alternative(get_template(j[3]).render(context), "text/html")

        try:
            message.send(fail_silently=False)
            print('STATUS: Email sent')

        except:
            print('STATUS: Email failed')


def send_multi_sms(context, sms_templates):

    # import pdb; pdb.set_trace()
    for j in sms_templates:        
        
        try:
            msg= settings.client.messages.create(
                from_ = '+16137048511',
                to = j[0],
                body = get_template(j[1]).render(context)
            )
            print('STATUS: SMS sent')
            print(msg)

        except:
            print('STATUS: SMS failed')
            print(msg)


def appointment_task_context(appointment, hours_before, task_name):
    
    now = get_local_time(appointment)
    then = appointment.appointment_time.replace(tzinfo=None) ##datetime.datetime(2020, 5, 29, 15, 0, tzinfo=<UTC>)
    timer_dt = then - timedelta(hours = hours_before) - now

    context = {
        # 'timer': 10,
        'timer': int(timer_dt.total_seconds()),
        'notification_name': "{0}_notifications".format(task_name),
        'date_now': now.strftime("%d-%m-%Y %H:%M:%S"),
        'date_then': then.strftime("%d-%m-%Y %H:%M:%S"),
        'date_send': (now + timer_dt).strftime("%d-%m-%Y %H:%M:%S")
    }

    return context
    



