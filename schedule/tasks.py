from django.core.exceptions import ObjectDoesNotExist
from celery.contrib.abortable import AbortableTask
import celery
from time import sleep
from django.core.mail import send_mail
from accounts.models import User
from schedule.models import Prospect, Appointment, Manager, TaskHistory
from datetime import datetime, timedelta
import os
from schedule.methods import send_multi_emails, send_multi_sms, appointment_context, appointment_task_context
import urllib.parse


## EMAIL NOTIFICATION
from django.template import Context
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives

## SMS NOTIFICATION
from twilio.rest import Client
from RentQ3 import settings



# celery -A RentQ3  worker -l info -P gevent


@celery.shared_task(base=AbortableTask)
def send_reminder_task(appointment_id):
    # import pdb; pdb.set_trace()
    appointment = Appointment.objects.get(id=appointment_id)
    hours_before = Manager.objects.filter(staff=appointment.staff.pk)[0].reminder_timer
    task_name = 'reminder'
    task = appointment_task_context(appointment, hours_before, task_name)
    taskhistory = TaskHistory.objects.get_or_create(task_name=task['notification_name'])[0]

    ### Prevent from sending reminder if we passed the send time (buffer)
    if task['timer'] < 0: 
        taskhistory.history.update({appointment_id:[{"Timestamp": task['date_now'], "Task id": celery.current_task.request.id, "Viewing": task['date_then'], "Timer": "0", 'Send task': "null", 'status': "Aborted (late registration)"}]})
        taskhistory.save()
        # celery.current_task.is_aborted
        print('Task [{0}] aborted due to LATE REGISTRATION...'.format(str(celery.current_task.request.id)))
        pass     
    else:
        taskhistory.history.update({appointment_id:[{"Timestamp": task['date_now'], "Task id": celery.current_task.request.id, "Viewing": task['date_then'], "Timer": str(task['timer']), 'Send task': task['date_send'], 'status': "Pending"}]})
        taskhistory.save()

        ### 3) Sleep
        sleep(task['timer'])

        ### 4) Execute the task
        try:
            appointment_now = Appointment.objects.get(id=appointment_id)
            context = appointment_context(appointment_now)

            if appointment_now.appointment_time != appointment.appointment_time or appointment_now.staff != appointment.staff:
                print('Task [{0}] aborted due to RESCHEDULING/STAFF REASSIGNEMENT...'.format(str(celery.current_task.request.id)))
                pass
            else:
                ### i) Send emails
                prospect_email = [
                    [appointment_now.prospect.email],
                    'Reminder! Viewing {0} ({1})'.format(context['TODAY_REFORMAT'], context['SHORT_ADDRESS']),
                    'schedule/reminder_email_prospect.txt',
                    'schedule/reminder_email_prospect.html'
                ]
                staff_email = [
                    [appointment_now.staff.user.email],
                    'Reminder! Viewing {0} ({1})'.format(context['TODAY_REFORMAT'], context['SHORT_ADDRESS']),
                    'schedule/reminder_email_staff.txt',
                    'schedule/reminder_email_staff.html'
                ]
                templates_array = [prospect_email, staff_email]

                send_multi_emails(context, templates_array)

                taskhistory.history.update({appointment_id:[{"Timestamp": task['date_now'], "Task id": celery.current_task.request.id, "Viewing": task['date_then'], "Timer": "0", 'Send task': task['date_send'], 'status': "Sent"}]})
                taskhistory.save()

        except ObjectDoesNotExist:
            taskhistory.history.update({appointment_id:[{"Timestamp": task['date_now'], "Task id": celery.current_task.request.id, "Viewing": task['date_then'], "Timer": "0", 'Send task': "null", 'status': "Aborted (cancellation)"}]})
            taskhistory.save()
            print('Task [{0}] aborted due to CANCELLATION...'.format(str(celery.current_task.request.id)))

    return None


@celery.shared_task(base=AbortableTask)
def send_rsvp_task(appointment_id):
    # import pdb; pdb.set_trace()
    appointment = Appointment.objects.get(id=appointment_id)
    hours_before = Manager.objects.filter(staff=appointment.staff.pk)[0].rsvp_timer ## minutes before the viewing
    task_name = "rsvp"
    task = appointment_task_context(appointment, hours_before, task_name)
    taskhistory = TaskHistory.objects.get_or_create(task_name=task['notification_name'])[0]

    ### Prevent from sending RSVP if we passed the send time (buffer)
    if task['timer'] < 0: 
        taskhistory.history.update({appointment_id:[{"Timestamp": task['date_now'], "Task id": celery.current_task.request.id, "Viewing": task['date_then'], "Timer": "0", 'Send task': "null", 'status': "Aborted (late registration)"}]})
        taskhistory.save()
        print('Task [{0}] aborted due to LATE REGISTRATION...'.format(str(celery.current_task.request.id)))
        pass
    else:
        taskhistory.history.update({appointment_id:[{"Timestamp": task['date_now'], "Task id": celery.current_task.request.id, "Viewing": task['date_then'], "Timer": str(task['timer']), 'Send task': task['date_send'], 'status': "Pending"}]})
        taskhistory.save()

        ### 3) Timer
        sleep(task['timer'])
        
        ### 4) Execute the task
        try:

            # send_or_abort(apointment_now, appointment, email_templates, sms_templates)

            appointment_now = Appointment.objects.get(id=appointment_id)
            context = appointment_context(appointment_now, buffer=hours_before*60)

            if appointment_now.appointment_time != appointment.appointment_time or appointment_now.staff != appointment.staff:
                print('Task [{0}] aborted due to RESCHEDULING/STAFF REASSIGNEMENT...'.format(str(celery.current_task.request.id)))
                pass
            else:
                ### i) Send email
                prospect_email = [
                    [appointment_now.prospect.email],
                    'Action Required: Confirm your viewing {0} ({1})'.format(context['TODAY_REFORMAT'], context['SHORT_ADDRESS']),
                    'schedule/rsvp_request_sms_prospect.txt',
                    'schedule/rsvp_request_email_prospect.html'
                ]
                email_templates = [prospect_email]
            
                send_multi_emails(context, email_templates)

                ### ii) Send SMS
                prospect_sms = [
                    [appointment_now.prospect.phone],
                    'schedule/rsvp_request_sms_prospect.txt'
                ]
                sms_templates = [prospect_sms]
                
                send_multi_sms(context, sms_templates)

                taskhistory.history.update({appointment_id:[{"Timestamp": task['date_now'], "Task id": celery.current_task.request.id, "Viewing": task['date_then'], "Timer": "0", 'Send task': task['date_send'], 'status': "Sent"}]})
                taskhistory.save()

        except ObjectDoesNotExist:
            taskhistory.history.update({appointment_id:[{"Timestamp": task['date_now'], "Task id": celery.current_task.request.id, "Viewing": task['date_then'], "Timer": "0", 'Send task': "null", 'status': "Aborted (cancellation)"}]})
            taskhistory.save()
            print('Task [{0}] aborted due to CANCELLATION...'.format(str(celery.current_task.request.id)))


    return None
