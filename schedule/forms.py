from django.forms import ModelForm, DateInput, DateTimeInput, HiddenInput
from schedule.models import Availability, Appointment
from crm.models import Staff, Unit, Manager
from datetime import datetime
from django.forms import DateTimeField, DateField
from RentQ3.middleware import get_current_user


class NewAvailabilityForm(ModelForm):
    start_time = DateTimeField(required=True, input_formats=['%H:%M'], localize=True, widget=DateTimeInput(attrs={'placeholder': 'hh:mm'}))
    end_time = DateTimeField(required=True, input_formats=['%H:%M'], localize=True, widget=DateTimeInput(attrs={'placeholder': 'hh:mm'} ))
    selected_date = DateField(required=True)
    

    # import pdb; pdb.set_trace()

    class Meta:
        model = Availability
        fields = ['staff', 'unit', 'start_time', 'end_time', 'selected_date']

    def __init__(self, user, *args, **kwargs):
        # import pdb; pdb.set_trace()
        super(NewAvailabilityForm, self).__init__(*args, **kwargs)
        if user.is_manager:
            # Only display staff associated to the the manager (ie, active user)
            self.fields['staff'].queryset = Staff.objects.filter(manager=user.id)
            # Only display properties associated to the the manager (ie, active user) and preselect them
            self.fields['unit'].queryset = Unit.objects.filter(manager=user.id)
            self.fields['unit'].initial = Unit.objects.filter(manager=user.id)
        else:
            # Set initial value of staff to active user
            self.fields['staff'].initial = Staff.objects.get(user=user.id)
            # Only display properties associated to the staff and preselect them
            self.fields['unit'].queryset = Unit.objects.filter(staff=user.id)
            self.fields['unit'].initial = Unit.objects.filter(staff=user.id)

    def clean(self):
        # import pdb; pdb.set_trace()
        end_time = self.cleaned_data['end_time']
        selected_date = self.cleaned_data['selected_date']
        end_time = end_time.replace(year = self.cleaned_data['selected_date'].year)
        end_time = end_time.replace(month = self.cleaned_data['selected_date'].month)
        end_time = end_time.replace(day = self.cleaned_data['selected_date'].day)

        start_time = self.cleaned_data['start_time']
        start_time = start_time.replace(year = self.cleaned_data['selected_date'].year)
        start_time = start_time.replace(month = self.cleaned_data['selected_date'].month)
        start_time = start_time.replace(day = self.cleaned_data['selected_date'].day)

        return self.cleaned_data.update({'end_time': end_time, 'start_time': start_time})



class UpdateAvailabilityForm(ModelForm):

    class Meta:
        model = Availability
        fields = '__all__'


    def __init__(self, user, *args, **kwargs):
        # import pdb; pdb.set_trace()
        super(UpdateAvailabilityForm, self).__init__(*args, **kwargs)
        if user.is_manager:
            # Only display staff associated to the the manager (ie, active user)
            self.fields['staff'].queryset = Staff.objects.filter(manager=user.id)
            # Only display properties associated to the the manager (ie, active user)
            self.fields['unit'].queryset = Unit.objects.filter(manager=user.id)
        else:
            # Set initial value of staff to active user
            self.fields['staff'].initial = Staff.objects.get(user=user.id)
            # Only display properties associated to the staff
            self.fields['unit'].queryset = Unit.objects.filter(staff=user.id)


class UpdateAppointmentForm(ModelForm):

    class Meta:
        model = Appointment
        # fields = '__all__'
        exclude = ('prospect', 'unit', 'status')
