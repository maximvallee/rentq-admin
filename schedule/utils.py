import datetime
from calendar import HTMLCalendar
from schedule.models import Availability
from crm.models import Manager
from accounts.models import User
from django.urls import reverse
from RentQ3.middleware import get_current_user

class Calendar(HTMLCalendar):
	def __init__(self, year=None, month=None):
		self.year = year
		self.month = month
		super(Calendar, self).__init__()

	def formatday(self, day, availabilities):
		availabilities_per_day = availabilities.filter(start_time__day=day)
		d = ''
		pk = ''
		for availabilitiy in availabilities_per_day:
			d += f'<li> {availabilitiy.get_html_url} </li>'


		if day != 0:
			# import pdb; pdb.set_trace()
			url = reverse('schedule:availability_new')
			today = datetime.date.today()
			dayRendered = datetime.date(self.year, self.month, day)
			if today == dayRendered:
				dayLabel = "TODAY"
			else:
				dayLabel = day

			return f'<td><a href="{url}?date={self.year}-{self.month}-{day}">{dayLabel}&nbsp;</a><ul> {d} </ul></td>'

		return '<td></td>'

# <a href="#"></a>

	def formatweek(self, theweek, availabilities):
		week = ''
		for d, weekday in theweek:
			week += self.formatday(d, availabilities)
		return f'<tr> {week} </tr>'

	def formatmonth(self, withyear=True):
		# import pdb; pdb.set_trace()
		current_user = get_current_user()
		# availabilities = Availability.objects.filter(start_time__year=self.year, start_time__month=self.month)
		if current_user.is_manager:
			availabilities = Availability.objects.filter(start_time__year=self.year, start_time__month=self.month).filter(staff__manager=Manager.objects.get(user=current_user))
		else:
			availabilities = Availability.objects.filter(start_time__year=self.year, start_time__month=self.month).filter(staff__user=User.objects.get(id=current_user.id))

		cal = f'<table border="0" cellpadding="0" cellspacing="0" class="calendar">\n'
		cal += f'{self.formatmonthname(self.year, self.month, withyear=withyear)}\n'
		cal += f'{self.formatweekheader()}\n'
		for week in self.monthdays2calendar(self.year, self.month):
			# import pdb; pdb.set_trace()
			cal += f'{self.formatweek(week, availabilities)}\n'
		return cal
