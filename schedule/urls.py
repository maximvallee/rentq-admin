from django.urls import path
from schedule import views

app_name = 'schedule'

urlpatterns = [
    path('', views.CalendarView.as_view(), name='calendar'),
    path('availability/new/', views.AvailabilityCreateView.as_view(), name='availability_new'),
    path('availability/<int:pk>/edit/', views.AvailabilityUpdateView.as_view(), name='availability_edit'),
    path('availability/<int:pk>/delete/', views.AvailabilityDeleteView.as_view(), name='availability_delete'),
    path('appointments/', views.AppointmentListView.as_view(), name='appointment_list'),
    path('appointments/<int:pk>/cancel/', views.AppointmentDeleteView.as_view(), name='appointment_delete'),
    path('appointments/<int:pk>/update/', views.AppointmentUpdateView.as_view(), name='appointment_update'),
    path('appointments/cancelled/', views.AppointmentDeleteConfirmedView.as_view(), name='appointment_delete_confirmed'),
    path('status/<int:pk>/', views.rsvp, name='rsvp'),
    path('appointments/<int:pk>/reminder/', views.send_reminder, name='reminder_send'),
    path('appointments/<int:pk>/rsvp/', views.send_rsvp, name='rsvp_send'),
]
