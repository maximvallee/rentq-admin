from django.db import models
from django.urls import reverse
from accounts.models import User
from crm.models import Manager, Staff, Unit, Prospect

##Task Model
from django.utils.translation import ugettext_lazy as _
import jsonfield



class Availability(models.Model):
    staff = models.ForeignKey(Staff, on_delete=models.CASCADE)
    unit = models.ManyToManyField('crm.Unit')
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

    class Meta:
        verbose_name_plural = "Availabilities"
        ordering = ('start_time', )

    def get_absolute_url(self):
        return reverse('schedule:calendar')

    def __str__ (self):
        return "{:d}:{:02d}".format(self.start_time.hour, self.start_time.minute) + " - " + "{:d}:{:02d}".format(self.end_time.hour, self.end_time.minute)

    @property
    def get_html_url(self):
        url = reverse('schedule:availability_edit', args=(self.id,))
        return f'<a href="{url}"> {self.staff} · {"{:d}:{:02d}".format(self.start_time.hour, self.start_time.minute) + " - " + "{:d}:{:02d}".format(self.end_time.hour, self.end_time.minute)} </a>'

    @property
    def is_super_staff(self):
        # import pdb; pdb.set_trace()
        if Unit.objects.filter(staff=self.staff).count() > 1:
            return "Superstaff"
        else:
            return "Staff"


class Appointment(models.Model):

    APPOINTMENT_TYPE_CHOICES = (
        ('zoom', 'Zoom'),
        ('in_person', 'In person'),
    )

    appointment_time = models.DateTimeField()
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    staff = models.ForeignKey(Staff, on_delete=models.CASCADE)
    prospect = models.ForeignKey(Prospect, on_delete=models.CASCADE)
    appointment_type = models.CharField(max_length=40, choices=APPOINTMENT_TYPE_CHOICES, default='in_person')
    status = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse('schedule:appointment_list')

class TaskHistory(models.Model):
    # Relations
    # Attributes - mandatory
    task_name = models.CharField(
        max_length=100,
        verbose_name=_("Task name"),
        help_text=_("Select a task to record"),
        )
    # Attributes - optional
    history = jsonfield.JSONField(
        default={},
        verbose_name=_("history"),
        help_text=_("JSON containing the tasks history")
        )
    # Manager
    # Functions

    # Meta & unicode
    class Meta:
        verbose_name = _('Task History')
        verbose_name_plural = _('Task Histories')

    def __unicode__(self):
        return _("Task History of Task: %s") % self.task_name
    