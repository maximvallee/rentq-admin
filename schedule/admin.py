from django.contrib import admin
from schedule.models import Availability, Appointment, TaskHistory

# Register your models here.

class AvailabilityAdmin(admin.ModelAdmin):
    list_display = ['id', 'staff', 'start_time', 'end_time']
    list_editable = ['start_time', 'end_time']


class AppointmentAdmin(admin.ModelAdmin):
    list_display = ['id', 'status', 'appointment_time', 'unit', 'appointment_type', 'staff', 'prospect']
    list_editable = ['status', 'appointment_time']

class TaskHistoryAdmin(admin.ModelAdmin):
    list_display = ['id','task_name']
    class Meta:
        TaskHistory


admin.site.register(Availability, AvailabilityAdmin)
admin.site.register(Appointment, AppointmentAdmin)
admin.site.register(TaskHistory, TaskHistoryAdmin)
