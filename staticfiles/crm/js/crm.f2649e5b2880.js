window.addEventListener('load', function () {


    function fade(element) {
        var op = 1;  // initial opacity
        var timer = setInterval(function () {
            if (op <= 0.1){
                clearInterval(timer);
                element.style.display = 'none';
            }
            element.style.opacity = op;
            element.style.filter = 'alpha(opacity=' + op * 100 + ")";
            op -= op * 0.1;
        }, 75);
    }


    let el = document.querySelector(".container");
    if (el){
        el.addEventListener('click', (e) =>{
            if (e.target.matches('.copyBtn')){
                let id = e.target.id.match(/(\d+)/)[0]
                let url = document.getElementById('calendarBtn-'+ id).href
                let dummy = document.createElement("textarea");
                document.body.appendChild(dummy);
                dummy.value = url;
                dummy.select();
                document.execCommand("copy");
                document.body.removeChild(dummy);
                let check = document.getElementById('check-'+ id)
                check.style.display = 'block'
                fade(check)
    
            }
        })
    }




  })

   