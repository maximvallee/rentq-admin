import Landing from  './models/Landing'
import Availability from './models/Availability'
import Dateline from  './models/Dateline'
import Timeline from  './models/Timeline'
import UserForm from  './models/UserForm'
import Disclaimer from  './models/Disclaimer'
// import Confirmation from  './models/Confirmation'
import * as Confirmation from './models/Confirmation'
import * as landingView from './views/landingView'
import * as datelineView from './views/datelineView'
import * as timelineView from './views/timelineView'
import * as userFormView from './views/userFormView'
import * as disclaimerView from './views/disclaimerView'
import * as confirmationView from './views/confirmationView'
import * as headingView from './views/headingView'
import * as error404View from './views/error404View'
import {elements, progress, loader, clearLoader} from './views/base'
import moment from 'moment'

//// TESTING ////
window.l = new Dateline()
window.l = new Timeline()
window.l = new UserForm()
window.l = new Landing()


////Adjust the 15 mins extra
const state = {};
var today = new Date()
const devMode = 'staging' //npm, dev, staging, prod
const unit = 1 // Get unit id from url

const controlAvailability = async () =>{

  state.availability = new Availability(unit, devMode);
  loader(elements.dateline)

  try {
    // Wait for the availabilities
  
    
    await state.availability.getAvailability() //Run the prototype getResults() on the new object, which will return 'result'. Result will be stored in 'result' property, as defined in the function Search.js
    clearLoader()
    headingView.RenderHeading(state.availability.unitNo, state.availability.address, state.availability.manager)
    headingView.renderLogo(state.availability.manager.logo)
    
    state.availability.validateClump()

    
    // Display Dateline if there are not existing appointments
    if (state.availability.isEmptyArray(state.availability.appointments)){
      controlDateline(today)
    // Display Datequeue if there are existing appointments
    } else {
      state.availability.mergeAppointments(state.availability.appointments)
      controlLanding()
    }


  } catch (err){
    console.log(err)
    error404View.render404Page()
    // alert('Error reaching API')
  }

  // 2) Find the index of dates that have availability
}




//////////////// DATEQUEUE CONTROLLER ////////////////

const controlLanding = () =>{

  

  // 1) Create a new Dateline object
  state.landing = new Landing();

  // 2) Generate list of already scheduled open-houses


  

  // 3) Render queue
  // state.availability.getUtcOffset()
  datelineView.clearDateline()
  landingView.updateProgress(progress.level0)
  landingView.renderLanding(state.availability.mergedAppointments)
  headingView.renderInstruction(headingView.getViewingType(state.availability.appointmentType))
  datelineView.scrollTop()
  state.availability.getUtcOffset()
}


//////////////// TIMEQUEUE CONTROLLER ////////////////

const controlTimeQueue = (selectedID) => {

  // 1) Create a new Timeline object
  if (typeof selectedID === 'undefined') { //If user is returning to page from UserForm Page, then no need to reinstantiate the state
    state.timeline = new Timeline ()
  }

  // if (state.dateline){
  //   delete Dateline.selectedDay
  // }

  // 2) Generate an array of times between start time and end time

  state.availability.getIntervals(state.landing.selectedAppointment)

  // 3) Render the Timequeue
  timelineView.clearTimeline()
  landingView.clearLanding()
  datelineView.renderReturnBtn()
  landingView.updateProgress(progress.level2)
  headingView.renderInstruction("Pick a time")  
  timelineView.renderTimeline(state.landing.selectedDay, state.availability.intervals)
  
}


//////////////// DATELINE CONTROLLER ////////////////

const controlDateline = (anyDate) => {
  


    // 1) Create a new Dateline object
    state.dateline = new Dateline(anyDate);

    // 2) Generate an array of dates between start date and end date
    state.dateline.calcStartDate()
    state.dateline.calcEndDate()
    state.dateline.getDates()
    state.dateline.getPastDates()
    state.dateline.getTodayIndex()
    state.dateline.getMonth()
    

    // 3) Render the dateline
    landingView.clearLanding()
    landingView.updateProgress(progress.level1)
    headingView.renderInstruction(headingView.getViewingType(state.availability.appointmentType))
    datelineView.renderDateline(state.dateline)
    datelineView.highlightToday(state.dateline.todayIndex)
    datelineView.scrollTop()
    datelineView.clearReturnBtn()
    if (state.landing) {
        datelineView.renderMoreBtn()
    }
    

    // 4) Generate availability index and render inside the dateline
    ///// buffer availability ////
    state.availability.bufferAvailability(state.availability.availabilities)
    state.availability.getAvailabilityIndex(state.dateline)
    datelineView.renderAvailability(state.availability.availabilityIndex, state.dateline.todayIndex)

}


//////////////// TIMELINE CONTROLLER ////////////////
const controlTimeline = (selectedID) => {

    // 1) Create a new Timeline object
    if (typeof selectedID === 'undefined') { //If user is returning to page from UserForm Page, then no need to reinstantiate the state
      state.timeline = new Timeline ()
    }
  
    // 2) Generate an array of times between start time and end time
    state.availability.mergeTimes(state.dateline.selectedDay)
    state.availability.getIntervals(state.availability.mergedAvailability)

    // 3) Render the timeline
    timelineView.clearTimeline()
    datelineView.clearDateline()
    datelineView.renderReturnBtn()
    landingView.updateProgress(progress.level2)
    headingView.renderInstruction("Pick a time")
    timelineView.renderTimeline(state.dateline.selectedDay, state.availability.intervals)

}


//////////////// USERFORM CONTROLLER ////////////////

const controlUserForm = () => {

    // 1) Create a new userForm object


    // No need to reinstantiate the state if user is returning from Disclaimer page
    if (typeof state.userform === 'undefined') { 
      state.userform = new UserForm()
    }
    
    // 3) Validate and save the form data
    

    // 3) Render the userform
    timelineView.clearTimeline()
    landingView.updateProgress(progress.level3)
    headingView.renderInstruction("Enter your information")
    userFormView.renderUserForm()
    userFormView.displayAppointmentTypeDropdown(state.availability.appointmentType)
    datelineView.scrollTop()
}



//////////////// DISCLAIMER CONTROLLER ////////////////

const controlDisclaimer = () => {

  // 1) Create a new Disclaimer object

  // 2) Render the disclaimer

  userFormView.clearUserForm()
  landingView.updateProgress(progress.level4)
  headingView.renderInstruction("Heads up!")
  disclaimerView.renderDisclaimer(state.availability)
  datelineView.scrollTop()

}


//////////////// CONFIRMATION CONTROLLER ////////////////

const controlConfirmation = async () =>{

  // 1) Clear the disclaimer page and render the loader
  disclaimerView.clearDisclaimer()
  landingView.clearHeading()
  loader(elements.confirmation)

  try{
    // 2) Send POST request to server and wait before executing below
    await state.userform.postData(state.availability, state.timeline, devMode)

    // 3) Clear the loader and render the confirmation page
    clearLoader()
    confirmationView.fullViewPort()
    confirmationView.renderConfirmation(state.availability, state.timeline)
    confirmationView.renderSnackbar()
    setTimeout(confirmationView.hideSnackbar, 6000);
    google.maps.event.addDomListener(window, 'load', confirmationView.renderMap(state.availability.address));

  } catch(err) {
    console.log(err)
  }

}


//////////////// EVENT LISTENERS ////////////////

////// Upon Loading //////
window.addEventListener('load',() => {
  controlAvailability()
  console.log('mode:' + devMode)



})


////// Landing - Listeners //////


// Click Private viewings
elements.landing.addEventListener('click', e =>{
  if (e.target.matches('.more-times-text')){
    controlDateline(today)

// Click Clump Select //
  } else if (e.target.matches('.select-container-lg, .select-container-lg *')){
    state.landing.saveSelectedID(e)
    state.landing.saveSelectedDay(state.availability.mergedAppointments)
    state.landing.saveSelectedAppointment(state.availability.mergedAppointments)
    controlTimeQueue()    
    
  }
})

////// Dateline - Listeners //////

//Click Dateline date
elements.dateline.addEventListener('click', e =>{

  if (e.target.matches('.date, .date *') && (e.target.classList.contains("available"))) {
    state.dateline.getSelectedDay(state.dateline.getSelectedDateID(e))
    controlTimeline()
  } else if (e.target.matches('.more-times-text')) {

    // if (document.querySelector('.progress-label').textContent === `${progress.level1}%`){
      datelineView.clearReturnBtn()
      datelineView.clearDateline()
      controlLanding()
            


  }
    
})

////// Timeline - Listeners //////
elements.timeline.addEventListener('touchstart', e => {
  if (e.target.matches('.time-select-container, .time-select-container *')) {
      const id = e.target.id.match(/(\d+)/)[0]
      timelineView.highlightCell(id)
    }

}, { passive: true })

elements.timeline.addEventListener('touchend', e => {
  if (e.target.matches('.time-select-container, .time-select-container *')) {
      const id = e.target.id.match(/(\d+)/)[0]
      timelineView.clearHighlightCell(id)
    }

}, { passive: true })

elements.timeline.addEventListener('click', e => {

    // Click Time Select //
  if (e.target.matches('.time-select-container, .time-select-container *')) {
    if (state.timeline.selectedID){
      timelineView.clearConfirmBtn(state.timeline.selectedID)
    }
    const id = e.target.id.match(/(\d+)/)[0]
    state.timeline.saveSelectedTime(id, state.availability.intervals)
    timelineView.renderConfirmBtn(id)

    // Click Time Cancel //
  } else if (e.target.matches('.time-cancel, .time-cancel span')) {
    timelineView.clearConfirmBtn(state.timeline.selectedID)

    // Click Time Confirm //
  } else if (e.target.matches('.time-confirm, .time-confirm span')) {
    controlUserForm()
    state.timeline.assignAgent(state.timeline.selectedTime, state.availability.availabilities)

    // Prefill the userform if it had already been filled out 
    // if (typeof state.userform.first_name !== 'undefined'){
    if (localStorage.length > 1){
        state.userform.getFromLocalStorage()
        userFormView.prefillForm(state.userform)
    }
  }
})


////// User Form - Listeners //////

// Type
elements.userform.addEventListener('input', (e) => {

      // Validate 'null' on all inputs
      if (e.target.matches('.input-box')){
          userFormView.isEmpty(e.target)
      }

      // Validate phone input
      if (e.target.id ==='4'){
          userFormView.formatPhone(e.target)
          userFormView.isPhone(e.target)
          
      }

      // Validate email input
      if (e.target.id ==='3'){
          userFormView.isEmail(e.target)
      }
})


// Click Blue Btn
elements.userform.addEventListener('click', e => {
  if (e.target.matches('.btn-blue')){
    if (userFormView.formValidation()){
        state.userform.saveData()
        state.userform.saveToLocalStorage(state.userform)

        if (state.availability.instructions === undefined || state.availability.instructions.length == 0) { 
          controlConfirmation() 
        } else {
          controlDisclaimer() // If no instructions provided, skip Disclaimer page, straight to Confirmation page.
        }
    } 
  }
})



////// Disclaimer Form - Clicks //////

//Click Blue btn in disclaimer
elements.disclaimer.addEventListener('click', e => {
  if (e.target.matches('.btn-blue')){
    if (disclaimerView.disclaimerFormValidation()){
      controlConfirmation()
    }
  }
})


////// Confirmation - Clicks //////

//Click Blue btn in confirmation
elements.confirmation.addEventListener('click', e => {
  if (e.target.matches('.btn-blue')){
    confirmationView.clearConfirmation()
  }
})

document.querySelector(".closeSnackbar").addEventListener("click", confirmationView.closeSnackbar);




////// Heading - Clicks //////

//Click Return btn
elements.header.addEventListener('click', e =>{
  if (e.target.matches('.return-btn')){

        // Returning from Timeline
        if (document.querySelector('.progress-label').textContent === `${progress.level2}%`){
          timelineView.clearTimeline()
        
              //If user comes from Dateline
              if (state.dateline && typeof state.dateline.selectedDay !== "undefined"){
                controlDateline(today)
              } else {
                
              //If user comes from DateQueue
                controlLanding()
                datelineView.clearReturnBtn()
              }

        // Returning from UserForm
        } else if (document.querySelector('.progress-label').textContent === `${progress.level3}%`){
          userFormView.clearUserForm()

                //If user comes from Dateline
                if (state.dateline && typeof state.dateline.selectedDay !== "undefined"){ 
                  controlTimeline(state.timeline.selectedID)

                //If user comes from DateQueue
                } else { 
                  controlTimeQueue(state.timeline.selectedID)
                }

                timelineView.renderConfirmBtn(state.timeline.selectedID)
                timelineView.scrollToElement(state.timeline.selectedID)

        // Returning from Dislaimer
        } else if (document.querySelector('.progress-label').textContent === `${progress.level4}%`){
              disclaimerView.clearDisclaimer()
              controlUserForm()
              userFormView.prefillForm(state.userform)
        }
  }
})


// ////// Dateline - Clicks //////

// //Click Return btn
// elements.header.addEventListener('click', e =>{
//   if (e.target.matches('.return-btn')){


