import moment from 'moment'
export default class Landing {
  constructor(){

  }

  saveSelectedID(click){
    this.selectedID = click.target.id.match(/(\d+)/)[0]
  }

  saveSelectedDay(arrAppointments){    
    this.selectedDay = arrAppointments[this.selectedID]["start_time"]
  }

  saveSelectedAppointment(arrAppointments){
    let selectedAppointment = []
    selectedAppointment.push(arrAppointments[this.selectedID])
    this.selectedAppointment = selectedAppointment
  }

}
