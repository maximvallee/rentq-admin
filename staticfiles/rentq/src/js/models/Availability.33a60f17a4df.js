import axios from 'axios';
import moment from 'moment'
import 'moment-timezone'
import Dateline from  './Dateline'


export default class Availability {
  constructor(unit, devMode){
    // this.unit = (devMode) ? unit: window.location.pathname.split('/')[2];
    this.unit = window.location.pathname.split('/')[2];
    this.devMode = devMode
  }

  async getAvailability(){

    let api
    if (this.devMode){
      // api = `http://127.0.0.1:8000/api/unit/?id=3` // Local live JS testing through NPM
      api = `http://127.0.0.1:8000/api/unit/?id=${this.unit}` // Local API
    } else {
      api = `http://www.rentq.io/api/unit/?id=${this.unit}`
    }
    try{
      const res = await axios(api)
      this.unitNo = res.data[0].unit_no
      this.address = res.data[0].address
      this.manager = res.data[0].manager
      this.instructions = res.data[0].instruction
      this.availabilities = res.data[0].availability
      this.appointments = res.data[0].appointment
    } catch(error){
      alert(error)
    }
  }

  isEmptyArray(arr){
    if (arr.length === 0){
      return true
    } else {
      return false
    }
  }

  getAvailabilityIndex(dateline){
    console.log(this.availabilities)
    const dateRange = dateline.dateRange
    const arr = []
    const startBuffer = 1// How many hours away from Now do you want to allow scheduling (Dateline) (default: 36)
    const startCuttoff = moment(new Date()).add(startBuffer, 'hours')
    

    dateRange.forEach((e1, index1) => {
      this.availabilities.forEach((e2, index2)=> {

        // Get the available dates that are in the dateline array (ie, [1,3,5,10], [1,2,3,4,5,6,7] ----> [1,3,5])
        if (moment(e2.start_time).format('MMM Do YY') === moment(e1).format("MMM Do YY")){ 
            console.log((moment(e2.end_time).add(5, 'hours')).format('LLLL'))
            console.log(startCuttoff.format('LLLL'))
            console.log((moment(e2.end_time).add(5, 'hours')).isSameOrAfter(startCuttoff, 'minutes',[]))

            // Get availabilites that have an end time after the cutoff date (ie, cuttoff = 3, [1,3,5] ----> [5])
            if ((moment(e2.end_time).add(5, 'hours')).isSameOrAfter(startCuttoff, 'minutes',[])){ 
              arr.push(index1)
            }

            
        }
      })
    })
    
    this.availabilityIndex = arr
  }

  mergeTimes(selectedDay){
    let sameDay = []
    let stack = []
    let top = null
    let temp_avail = JSON.parse(JSON.stringify(this.availabilities)) // Because we are using this.availabilities, 
    // we are assigning not the object itself in the current state, but a reference to that object. We need to clone
    // the object so the state is frozen at that point.

    /// 1) Loop through all dates and merge same dates together []
    for (let i=0; i < temp_avail.length; i+=1){
      if (moment(moment(temp_avail[i].start_time)).isSame(moment(selectedDay),'day')) {
        sameDay.push(temp_avail[i])
      }
    }
  
    /// 2) Merge overlapping intervals

    // push the 1st interval into the stack
    stack.push(sameDay[0])

    // start from the next interval and merge if needed
    for (let i=0; i < sameDay.length; i+=1) {
      top = stack[stack.length -1]

      // if the current interval doesn't overlap with the 
      // stack top element, push it to the stack
      if (top.end_time < sameDay[i].start_time) {
        stack.push(sameDay[i])

      // otherwise update the end value of the top element
      // if end of current interval is higher
      } else if (top.end_time < sameDay[i].end_time) {
        top.end_time = sameDay[i].end_time 
        stack.pop()
        stack.push(top)
      }
    }
    
    for (let i=0; i < stack.length; i+=1){
      const now = moment().subtract(5, 'hours')
      // const now = moment("2020-01-22T16:30:00Z")
      let start_time = moment(stack[i].start_time)
      let end_time = moment(stack[i].end_time)
      console.log(now)
      console.log(start_time)
      console.log(end_time)

      if (now > end_time){
        console.log('delete time window')
        stack.splice(i, 1)  
      
      } else if (now > start_time){
        
        console.log('shrink time window')
        // stack[i].start_time = now
        // Round down Now to the closet 15-min interval (ie, [15:49] ---> [15:45])
        // stack[i].start_time = now.add(now.minutes() % 15, 'minutes')
        // console.log(50 % 15)
        let nextInterval = Math.ceil(new Date().getMinutes()/15)*15
        let toNextInterval = nextInterval  - moment().toDate().getMinutes()
        stack[i].start_time = now.add(toNextInterval, 'minutes')

        //moment().minutes() 
        
      }

    }
    
    this.mergedAvailability = stack
  }

  getIntervals(availability){
    const interval = 15
    const buffer = 15 // How many minutes away do you want to allow scheduling (Timeline)
    const now = moment.utc(new Date())
    let arrTimes = []
    

    // Loop through all same-date windows and merge them together
    for (let i=0; i < availability.length; i+=1){
      const endTime = moment.utc(new Date(availability[i].end_time))
      const startTime = moment.utc(new Date(availability[i].start_time))
      // const startTime = moment(new Date(availability[i].start_time)).tz("Europe/Lisbon")
      let newTime

      // Don't display passed times. Display from now, rounded up to the next interval
      // if (today.isSame(startTime, 'day')){
        
      if (startTime.isBefore(now, 'minutes')){

        // newTime = startTime.add(buffer, 'minutes')
        newTime = startTime
      } else {
        newTime = startTime
      }
      
      while (newTime.isBefore(endTime)){
        arrTimes.push(newTime)
        newTime = moment(newTime).add(interval, 'minutes')
        // newTime = moment(newTime).add(interval, 'minutes').tz("Europe/Lisbon")

        
      }
    }
    
    arrTimes = arrTimes.sort((a, b) => a - b);
    // console.log(arrTimes)
    arrTimes = arrTimes.filter( function( item, index, inputArray ) {
      // console.log(item)
      // console.log(index)
      return inputArray.indexOf(item) == index
    });
    // console.log(arrTimes)
    this.intervals = arrTimes
    console.log(this.mergedAvailability)
    console.log(this.intervals)
  }

  mergeAppointments(appointments){

    const threshold = moment.duration(1, 'hours') //How far from each other (in hours) can 2 appointments be that we don't clump them into one
    const interval = 15 //How far after the last appointment (in minutes) should we extend the availability window (CAREFUL: Can be out of range!)

    let arr1 = JSON.parse(JSON.stringify(this.appointments))
    let arr2 = [[moment.utc(arr1[0].appointment_time)]]
    let subNo = 0

    /// Clump array of times into multiple arrays of times near eachother (ie, [9,9.5,10,14] ---> [[9,9.5,10],[14])
    for (let i=1; i < arr1.length; i+=1) {
      let itemArr1 = moment.utc(arr1[i].appointment_time)
      let itemArr2 = arr2[subNo][arr2[subNo].length - 1]
      let duration = moment.duration(itemArr1.diff(itemArr2))

      if (duration <= threshold){
          arr2[subNo].push(itemArr1)
      } else {
        arr2.push([itemArr1])
        subNo+=1  
      }
    }

    /// Shrink all subarrays to 2 elements: start time & end time (ie, [[9,9.5,10],[14]] ---> [[9,10.25],[14,14.25]])
    for (let i=0; i < arr2.length; i+=1) {
        let startTime = arr2[i][0]
        let endTime = moment(arr2[i][arr2[i].length -1]).add(interval, 'minutes')
        
        arr2[i] = {}

        arr2[i]["start_time"] = startTime
        arr2[i]["end_time"] = endTime

    }

    this.mergedAppointments = arr2
    console.log(this.mergedAppointments)
  }

  validateClump(){
    console.log(this.availabilities)
    

    for (let i=0; i < this.appointments.length; i+=1) {
      let validated = false
      console.log(this.appointments)

      for (let j=0; j < this.availabilities.length; j+=1){
        let appointment = moment.utc(this.appointments[i]["appointment_time"])
        let availabilityStart = moment.utc(this.availabilities[j].start_time)
        let availabilityEnd = moment.utc(this.availabilities[j].end_time)

        if (appointment.isBetween(availabilityStart, availabilityEnd, 'minutes',[])){
          validated = true
        }
      }
      if (validated === false){
        console.log("Out of range")
        console.log(this.appointments[i])
        this.appointments.splice(i,1)
        i = i-1
      } 

    }
  }




}
