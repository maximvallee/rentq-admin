import moment from 'moment'
import axios from 'axios';
import * as disclaimerView from '../views/disclaimerView'
import * as confirmationView from '../views/confirmationView'
import * as landingView from '../views/landingView'
import {elements, progress, loader, clearLoader} from '../views/base'

export default class UserForm {
  constructor(){

  }

  saveData(){
    this.first_name = document.getElementById('1').value
    this.last_name = document.getElementById('2').value
    this.email = document.getElementById('3').value
    this.phone = document.getElementById('4').value
    this.unit_size = document.getElementById('5').value
    this.lease_term = document.getElementById('6').value
    this.occupation = document.getElementById('7').value
    this.appointment_type = document.querySelector('input[name="appointmentType"]:checked').value

    // Persist data in local storage (Browser)
    // this.persistData()

  }

  saveToLocalStorage(userform){
    localStorage.setItem('userform', JSON.stringify(userform))
    // localStorage.setItem('first_name', this.first_name)
    // localStorage.setItem('last_name', this.last_name)
    // localStorage.setItem('email', this.email)
    // localStorage.setItem('phone', this.phone)
    // localStorage.setItem('unit_size', this.unit_size)
    // localStorage.setItem('lease_term', this.lease_term)
    // localStorage.setItem('occupation', this.occupation)
     // localStorage.setItem('appointment_type', this.appointment_type)
  }

  getFromLocalStorage(){
    const firstName = JSON.parse(localStorage.getItem('userform')).first_name
    if (firstName) this.first_name = firstName

    const lastName = JSON.parse(localStorage.getItem('userform')).last_name
    if (lastName) this.last_name = lastName

    const email = JSON.parse(localStorage.getItem('userform')).email
    if (email) this.email = email

    const phone = JSON.parse(localStorage.getItem('userform')).phone
    if (phone) this.phone = phone

    const unitSize = JSON.parse(localStorage.getItem('userform')).unit_size
    if (unitSize) this.unit_size = unitSize

    const leaseTerm = JSON.parse(localStorage.getItem('userform')).lease_term
    if (leaseTerm) this.lease_term = leaseTerm

    const occupation = JSON.parse(localStorage.getItem('userform')).occupation
    if (occupation) this.occupation = occupation

    const appointmentType = JSON.parse(localStorage.getItem('userform')).appointment_type
    if (appointmentType) this.appointment_type = appointmentType
  }

  async postData(availability, timeline, devMode){

    let api

    if (devMode === 'npm'){
      api = 'http://127.0.0.1:8000/api/appointment/' // Local live JS testing through NPM (npm start + python manage.py runserver)
    } else if (devMode === 'dev') {
      api = 'http://127.0.0.1:8000/api/appointment/' // Local API (python manage.py runserver)
    } else if (devMode === 'staging'){
      api = 'https://rentqstaging.herokuapp.com/api/appointment/' // Heroku remote staging
    } else if (devMode === 'prod'){
      api = 'http://www.rentq.io/api/appointment/' // Heroku remote production
    } 


    let data = JSON.stringify({
      appointment_time: timeline.selectedTime._d,
      unit: !availability.unit ? 1 : availability.unit,
      staff: timeline.agent[0].user_id,
      prospect: {
          first_name: this.first_name,
          last_name: this.last_name,
          email: this.email,
          phone: this.phone,
          unit_size: this.unit_size,
          lease_term: this.lease_term,
          occupation: this.occupation,
      },
      appointment_type: this.appointment_type
    });

    try{
      const res = await axios.post(api,data,{headers:{"Content-Type" : "application/json"}})
      console.log(res)

    }catch(err){
      console.log(err)
    }
  }

}



