import moment from 'moment'
export default class Timeline {
  constructor(){

  }

  saveSelectedTime(id, intervals){
    this.selectedID = id
    this.selectedTime = intervals[id]
  }

  assignAgent(selectedTime, availabilities){

    const time = moment(selectedTime,'hour')
    let agent = []

    for (let i=0; i < availabilities.length; i+=1) {
      const start_time = moment(availabilities[i].start_time)
      const end_time = moment(availabilities[i].end_time)

      if (time.isBetween(start_time, end_time, null, [])){
        agent.push(availabilities[i].staff)
      } 
    this.agent = agent
    }
  }



}
