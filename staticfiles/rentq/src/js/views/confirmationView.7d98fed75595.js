import {elements} from './base'
import moment from 'moment'
import * as timelineView from './timelineView'

export const clearConfirmation = () => {
  elements.confirmation.innerHTML = ''
}

// export const clearLogo = () => {
//   const element = document.querySelector('.logo-container')
//   element.parentNode.removeChild(element);
// }

export const fullViewPort = () =>{
  const element = document.querySelector('.white-container')
  element.classList.add("height100vh")
}

export const renderConfirmation = (availability, timeline) => {
  const address = availability.address.split(',')[0]
  const agent = timeline.agent[0]
  const selectedDate = moment(timeline.selectedTime).format('ddd, MMM Do')
  const selectedTime = moment(timeline.selectedTime).format('h:mm a')

  const markup = `
        <div class="confirmation-container">
            <div class="confirmation-header">
                <div id="map"></div>
            </div>
            <div class="agent-thumbnail-container">
                <img class="agent-photo" src="http://127.0.0.1:8000/${agent.profile_pic}" alt="" onerror="this.onerror=null;this.src='https://rentq-app.s3.amazonaws.com/default-user.jpg';">
                <img class="agent-check" src="https://rentq-app.s3.amazonaws.com/checkmark-green-circle.svg" alt="">
            </div>
            <div class="confirmation-top">

                <h1 class="confirmation-header">Thanks for booking!</h1>

                <p class="date-confirmation-text">You scheduled a viewing with <br> <strong>${agent.name}</strong> on <strong>${selectedDate}</strong> at <strong>${selectedTime}</strong></p>
                <div class="address-confirmation-container">
                      <img class="location-icon" src="https://rentq-app.s3.amazonaws.com/location-marker.svg" alt="">
                      <span class="address-confirmation-text">${address}</span>
                      <img class="chevron-icon-right" src="https://rentq-app.s3.amazonaws.com/chevron-right-blue.svg" alt="">
                </div>
            </div>
        </div>
  `
  elements.confirmation.insertAdjacentHTML('beforeend', markup)
}


export const renderMap = (address) =>{
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    disableDefaultUI: true,
    zoomControl: true,
    styles: [{"featureType":"administrative","elementType":"labels","stylers":[{"hue":"#00b5ff"},{"saturation":100},{"lightness":34},{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#1c85e8"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#344066"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"saturation":-100},{"visibility":"on"},{"color":"#f4f8fb"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#b5ebe0"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#555b77"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"hue":"#deecec"},{"saturation":-73},{"lightness":72},{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"hue":"#bababa"},{"saturation":-100},{"lightness":25},{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#ffffff"},{"lightness":100},{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#71d6ff"},{"saturation":100},{"lightness":-5},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"##000000"}]}]
  });

  var geocoder = new google.maps.Geocoder();
  geocodeAddress(geocoder, map, address)
}

const geocodeAddress = (geocoder, resultsMap, address) =>{
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === 'OK') {
      resultsMap.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
        map: resultsMap,
        icon: "https://rentq-app.s3.amazonaws.com/marker.png",
        position: results[0].geometry.location
      });
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}


export const closeSnackbar = () =>{
  document.querySelector(".snackbar").style.display= "none";
}

export const renderSnackbar = () =>{
  const box = document.querySelector(".snackbar")
  var oppArray = ["0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1"];
    var x = 0;
    (function next() {
        box.style.opacity = oppArray[x];
        if(++x < oppArray.length) {
            setTimeout(next, 30); //depending on how fast you want to fade
        }
    })();
}




export const hideSnackbar = () =>{
  const box = document.querySelector(".snackbar")
  var oppArray = ["0.9", "0.8", "0.7", "0.6", "0.5", "0.4", "0.3", "0.2", "0.1", "0"];
    var x = 0;
    (function next() {
        box.style.opacity = oppArray[x];
        if(++x < oppArray.length) {
            setTimeout(next, 30); //depending on how fast you want to fade
        }
    })();
}


