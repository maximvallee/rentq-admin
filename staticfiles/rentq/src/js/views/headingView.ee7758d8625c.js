import {elements} from './base'
import moment from 'moment'

// export const RenderHeading = (address, manager) => {
//   document.querySelector(".header-text").innerHTML = address.split(',')[0]
//   document.querySelector(".logo-text").innerHTML = manager.company
//   document.querySelector(".logo").src = manager.logo
//   document.querySelector(".logo").setAttribute('data-before', manager.company.charAt(0))
// }

export const RenderHeading = (unit, address, manager) => {
    
  const location = address.split(',')[0]
  const unitNo = unit ? unit + "-" : ""
  const company = manager.company
  const logo = manager.logo
  const firstLetter = manager.company.charAt(0)

  const markup = `
            <header class="logo-container">
                  <div class="logo-default">${firstLetter}</div>
                  <img class="logo" src="${logo}" style="display: none;">
                  <span class="logo-text">${company}</span>
            </header>

            <div class="heading">
                  <img class="return-btn" src="https://rentq-app.s3.amazonaws.com/arrow-left-blue.svg" alt="">
                  <h1 class="header-text">${unitNo}${location}</h1>
                  <div class="summary">
                      <div class="instruction-container">
                          <p class="instruction-label"></p>
                      </div>
                  </div>

                <div class="progress-bar">
                    <div class="progress-blue"></div>
                    <div class="progress-label"></div>
                </div>
            </div>
`
elements.header.insertAdjacentHTML('beforeend', markup)
}

export const renderLogo = (image) => { 
    if (image !== null){
        document.querySelector('.logo-default').style.display = 'none'
        document.querySelector('.logo').style.display = 'block'
    }
}



// export const renderLogo = (image) => { 
//   if (image !== null){
//     document.querySelector('.logo-default').style.content = `url(${image})`
//     // document.querySelector('.logo').style.background = 'none'
//   }
// }

export const renderInstruction = (title) => {
    document.querySelector('.instruction-label').textContent = title
}
