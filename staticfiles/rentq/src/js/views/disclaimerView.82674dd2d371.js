import {elements} from './base'
import moment from 'moment'

export const clearDisclaimer = () => {
  elements.disclaimer.innerHTML = ''
}

const boldify = (text) => {
    const numerical = /\d+/;
    const words = text.split(" ")
    let word
    let answer =  ""
    for (var i = 0; i < words.length; i +=1){
        if (words[i].match(numerical) !== null || words[i].match("hour") !== null || words[i].match("minute") !== null){
            word = words[i].bold()
        } else {
            word = words[i]
        }
        answer = answer + " " + word
    }
    return(answer)
}

export const renderDisclaimer = (availability) => {
    const instructions = availability.instructions
    const markup = `
            <form>
                <div class="content-container scroll-margin">
                    <div class="scrollbox">
                        <div class="condition-container">
                            ${instructions.map((el, index) => createInstruction(el, index)).join(' ')}
                        </div>
                    </div>
                    <button type="button" name="button" class="btn-blue">Schedule viewing</button>
                    
                </div>
            </form>
    `
    elements.disclaimer.insertAdjacentHTML('beforeend', markup)
}

const createInstruction = (el, index) => `
            <div class="condition">
                <label class="checkbox-container">
                    <input class="checkbox" type="checkbox" id="disclaimer-${index}"required>
                    <span class="checkmark"></span>
                </label>
                <label for="disclaimer-${index}">
                    <div class="clause-container">
                        <h3 class="title-clause">${el.name}</h3>
                        <p class="text-clause">${boldify(el.value)}</p>
                    </div>
                </label>

                
            </div>
        `
export const disclaimerFormValidation = () => {
        const checkbox = document.getElementsByClassName("checkbox")
        let error = []
        for (let i=0; i < checkbox.length; i++){
            if (checkbox[i].checked !== false){
                document.getElementsByClassName("checkmark")[i].style.border = "1px solid #dedede";
            } else {
                document.getElementsByClassName("checkmark")[i].style.border = "1px solid #ff0000";
                document.getElementsByClassName("checkmark")[i].style.animation = "shake 0.5s"
                
                error.push(document.getElementsByClassName("checkmark")[i])
            }
        }

        if (error.length === 0){
            return true
        } else {
            error[0].scrollIntoView({block: "center"})
            return false
  
        }
}
