import {elements} from './base'

export const render404Page = () => {
    const markup = `
            <div class='error-container'>
                <div class='error-message'>
                    <img src="https://nxcso.stripocdn.email/content/guids/CABINET_a6dff9156d2588278d2a40b699366626/images/73781591213889783.png">
                    <h4>Sorry! This URL is not valid.</h4>
                </div>
             </div>
    `
    elements.landing.insertAdjacentHTML('beforeend', markup)
}