import {elements} from './base'
import moment from 'moment'

export const clearUserForm = () => {
  elements.userform.innerHTML = ''
}

export const renderUserForm = () => {
  const markup = `
      <form class="content-container input-form" action="index.html" method="post">
          <div class="scrollbox">
              <div class="input-container">
                  <div class="input-block">
                    <input type="text" name="firstname" value="" id="1" class="input-box" placeholder="First name" maxlength="30" required>
                    <img src="" id="validation-1" class="validation-icon" alt="">
                  </div>
              </div>
              <div class="input-container">
                  <div class="input-block">
                    <input type="text" name="lastname" value="" id="2" class="input-box" placeholder="Last name" maxlength="30" required>
                    <img src="" id="validation-2" class="validation-icon" alt="">
                  </div>
              </div>
              <div class="input-container">
                  <div class="input-block">
                    <input type="text" name="email" value="" id="3" class="input-box" placeholder="Email" maxlength="40" required>
                    <img src="https://rentq-app.s3.amazonaws.com/invalid-red.svg" id="invalid-3" class="validation-icon hidden" alt="">
                    <img src="https://rentq-app.s3.amazonaws.com/valid-green.svg" id="valid-3" class="validation-icon hidden" alt="">
                  </div>
              </div>
              <div class="input-container">
                  <div class="input-block">
                    <input type="text" name="phone" value="" id="4" class="input-box" placeholder="Phone" minlength="7" maxlength="19"required>
                    <img src="https://rentq-app.s3.amazonaws.com/invalid-red.svg" id="invalid-4" class="validation-icon hidden" alt="">
                    <img src="https://rentq-app.s3.amazonaws.com/valid-green.svg" id="valid-4" class="validation-icon hidden" alt="">
                  </div>
              </div>
              <hr class="divider-thin">
              <div class="input-container">
                  <div class="label-block">
                    <label for="number-rooms">
                      <span>Type of rental</span>
                    </label>
                  </div>
                  <div class="input-block">
                    <select id="5" class="input-box grab" name="number-rooms" required>
                      <option value="" disabled selected>Select your option...</option>
                      <option value="Private bedroom">Private bedroom</option>
                      <option value="Studio">Studio</option>
                      <option value="1-bedroom unit">1-bedroom unit</option>
                      <option value="2-bedroom unit">2-bedroom unit</option>
                      <option value="3-bedroom unit">3-bedroom unit</option>
                      <option value="4-bedroom unit">4-bedroom unit</option>
                      <option value="5-bedroom unit">5-bedroom unit</option>
                      <option value="+6-bedroom unit">+6-bedroom unit</option>
                    </select>
                    <img src="" id="validation-5" class="validation-icon" alt="">
                  </div>
              </div>
              <div class="input-container">
                  <div class="label-block">
                    <label for="term">
                      <span>Ideal length of stay</span>
                    </label>
                  </div>
                  <div class="input-block">
                    <select id="6" class="input-box grab" name="term" required>
                      <option value="" disabled selected>Select your option...</option>
                      <option value="4">4 months</option>
                      <option value="8">8 months</option>
                      <option value="12">12 months</option>
                    </select>
                    <img src="" id="validation-6" class="validation-icon" alt="">
                  </div>
              </div>
              <div class="input-container">
              <div class="label-block">
                <label for="occupation">
                  <span>Occupation</span>
                </label>
              </div>
              <div class="input-block">
                <select id="7" class="input-box grab" name="occupation" required>
                  <option value="" disabled selected>Select your option...</option>
                  <option value="Student">Student</option>
                  <option value="Young professional">Young professional</option>
                  <option value="Mature professional">Mature professional</option>
                  <option value="Retiree">Retiree</option>
                  <option value="Unemployed">Unemployed</option>
                </select>
                <img src="" id="validation-7" class="validation-icon" alt="">
              </div>
            </div>

            <div class="input-container" id="appointmentType" style='display:none'>
                <div class="label-block">
                    <label for="appointment-type">
                      <span>Viewing in person or online?</span>
                    </label>
                </div>
                <div class="input-block" id='8' style='text-align: left; font-size: 1.4rem'>
                    <input id="in-person" type="radio" value="in_person" name="appointmentType" checked>
                    <label for="in-person" style='cursor:pointer'>In person</label>
                    <input id="zoom" type="radio" value="zoom" name="appointmentType">
                    <label for="zoom" style='cursor:pointer'>Zoom</label>
                    <img src="" id="validation-8" class="validation-icon" alt="">
                </div>
            </div>

            </div>
          <button type="button" name="button" class="btn-blue grab">Next</button>
      </form>
  `
  elements.userform.insertAdjacentHTML('beforeend', markup)
}


export const displayAppointmentTypeDropdown = (appointmentType) => {    
  if (appointmentType == "zoom"){
    document.querySelector('input[name="appointmentType"]:checked').value = 'zoom'
  } else if (appointmentType == "in_person"){
    document.querySelector('input[name="appointmentType"]:checked').value = 'in_person'
  } else {
    document.getElementById('appointmentType').style.display='block'
  }
}

export const prefillForm = (userform) => {
    document.getElementById("1").value = userform.first_name
    document.getElementById("2").value = userform.last_name
    document.getElementById("3").value = userform.email
    document.getElementById("4").value = userform.phone
    document.getElementById("5").value = userform.unit_size
    document.getElementById("6").value = userform.lease_term
    document.getElementById("7").value = userform.occupation
    userform.appointment_type == 'zoom' ? document.getElementById('zoom').checked = true : document.getElementById('in-person').checked = true 
}


export const formValidation = () => {
    const inputBoxes = document.getElementsByClassName("input-box")
    let error = []
    for (let i=0; i < inputBoxes.length; i++){
      if (isEmpty(inputBoxes[i])){
            error.push(inputBoxes[i])
      }
      if (inputBoxes[i].id === '3'){
        if (isEmail(inputBoxes[i]) === false){
            error.push(inputBoxes[i])
        }    
      } else if (inputBoxes[i].id === '4'){
        formatPhone(inputBoxes[i])
        if (isPhone(inputBoxes[i]) === false){
            error.push(inputBoxes[i])
        }    
      }
    }
    
    if (error.length === 0){
      // console.log(error)
      return true
    } else {
      error[0].scrollIntoView({block: "center"})
      // console.log(error)
      return false 
    }
}

export const isEmpty = (inputField) => {
    if (inputField.value){
      removeErrorStyle(inputField)
      return false
    } else {
      addErrorStyle(inputField)
      return true
    }
}

export const isPhone = (phoneField) => {
    if (/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(phoneField.value)){
      removeErrorStyle(phoneField)
      return true
    } else {
      addErrorStyle(phoneField)
      return false
    }
}

export const isEmail = (emailField) => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailField.value)){
      removeErrorStyle(emailField)
      return true
    } else {
      addErrorStyle(emailField)
      return false
    }
}

export const formatPhone = (phoneField) => {
    let numbers = phoneField.value.replace(/\D/g, ''),
    char = {0:'(',3:') ',6:'-'};
    phoneField.value = '';
    for (var i = 0; i < numbers.length; i++) {
      phoneField.value += (char[i]||'') + numbers[i];
    }
}

const addErrorStyle = (field) => {
    field.style.border = "1px solid rgb(254,64,66)";
    // field.style.animation = "shake 0.5s"
    if (field.id === '3' || field.id === '4'){
      document.getElementById(`invalid-${field.id}`).classList.add("visible")
      document.getElementById(`valid-${field.id}`).classList.remove("visible")
    }
}

const removeErrorStyle = (field) => {
    field.style.border = "1px solid #dedede";
    if (field.id === '3' || field.id === '4'){
      document.getElementById(`invalid-${field.id}`).classList.remove("visible")
      document.getElementById(`valid-${field.id}`).classList.add("visible")
    }
}

