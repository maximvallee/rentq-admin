window.addEventListener('load', function () {

    phoneBox = document.getElementById('id_phone')

    const formatPhone = (phoneField) => {
        let numbers = phoneField.value.replace(/\D/g, ''),
        char = {0:'(',3:') ',6:'-'};
        phoneField.value = '';
        for (var i = 0; i < numbers.length; i++) {
          phoneField.value += (char[i]||'') + numbers[i];
        }
    }
    
    phoneField.addEventListener('input', (e) =>{
        formatPhone(phoneBox)
    })




})