window.addEventListener('load',() => {

    function renderMap() {
        
        // Display map without marker
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 14,
          disableDefaultUI: true,
          zoomControl: true,
          center: {lat: 45.425, lng: -75.692},
          styles: [{"featureType":"administrative","elementType":"labels","stylers":[{"hue":"#00b5ff"},{"saturation":100},{"lightness":34},{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#1c85e8"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#344066"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"saturation":-100},{"visibility":"on"},{"color":"#f4f8fb"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#b5ebe0"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#555b77"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"hue":"#deecec"},{"saturation":-73},{"lightness":72},{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"hue":"#bababa"},{"saturation":-100},{"lightness":25},{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#ffffff"},{"lightness":100},{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#71d6ff"},{"saturation":100},{"lightness":-5},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"##000000"}]}]
        });

        // Autocomplete search input field
        var autocomplete = new google.maps.places.Autocomplete((document.getElementById("id_address")), {
            types: ['address'],
            componentRestrictions: {
                country: "CA"
            }
        });

        // Display search results ordered according to map location
        autocomplete.bindTo('bounds', map);


        // Display marker straight when autocomplete
        autocomplete.addListener('place_changed', function() {

            var marker = new google.maps.Marker({
                map: map,
            });

            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
              // User entered the name of a Place that was not suggested and
              // pressed the Enter key, or the Place Details request failed.
              window.alert("No details available for input: '" + place.name + "'");
              return;
            }
        
            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
            } else {
              map.setCenter(place.geometry.location);
              map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
        })

        // Call geocodeAddress() to load marker from database
        var address = document.getElementById("id_address").value
        if (address){
            var geocoder = new google.maps.Geocoder();
            geocodeAddress(geocoder, map, address)
        }
       
    }

    // Display marker from database
    const geocodeAddress = (geocoder, resultsMap, address) =>{
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location,
            });
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
      
    renderMap()
    
 
});







  
// const geocodeAddress = (geocoder, resultsMap, address) =>{
//     geocoder.geocode({'address': address}, function(results, status) {
//       if (status === 'OK') {
//         console.log(address)
//         resultsMap.setCenter(results[0].geometry.location);
//         var marker = new google.maps.Marker({
//           map: resultsMap,
//           icon: "/img/marker3.png",
//           position: results[0].geometry.location
//         });
//       } else {
//         alert('Geocode was not successful for the following reason: ' + status);
//       }
//     });
//   }





