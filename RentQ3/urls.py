"""RentQ3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import handler400, handler500

from crm import views
from schedule.views import UnitViewSet, AppointmentViewSet

router = routers.DefaultRouter()
router.register(r'unit', UnitViewSet)
router.register(r'appointment', AppointmentViewSet)

urlpatterns = [
    path('', views.IndexView.as_view(), name='home'),
    path('admin/', admin.site.urls),
    path('', include("crm.urls", namespace="crm")),
    path('calendar/', include('schedule.urls')),
    path('accounts/', include("accounts.urls", namespace="accounts")),

    path('api-auth/', include('rest_framework.urls')),
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('api-token-auth/', obtain_auth_token)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# handler404 = 'crm.views.error_404_view'

handler404 = 'crm.views.handler404'
handler500 = 'crm.views.handler500'