export const elements = {
    heading: document.querySelector('.heading'),
    header: document.querySelector('.heading-section'),
    whiteContainer:document.querySelector('.content'),
    
    landing: document.querySelector('.landing-page'),
    dateline: document.querySelector('.dateline-page'),
    timeline: document.querySelector('.timeline-page'),
    userform: document.querySelector('.userform-page'),
    disclaimer: document.querySelector('.disclaimer-page'),
    confirmation: document.querySelector('.confirmation-page'),
    footer: document.querySelector('.footer')
}

export const progress = {
  level0: 20, // DateQueue
  level1: 20, // Dateline
  level2: 40, // Timeline, TimeQueue
  level3: 60, // Useform
  level4: 90 // Disclaimer
}


export const loader = (parent) => {
  const markup = `
          <div class="lds-ring">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
          </div>
  `
  parent.insertAdjacentHTML('afterend', markup)
}

export const clearLoader = () =>{
  const loader = document.querySelector('.lds-ring')
  if (loader){
    loader.parentElement.removeChild(loader)
  }
}



