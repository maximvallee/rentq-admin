import moment from 'moment'
export default class Dateline {
  constructor(anyDate){
    this.today = anyDate
  }

  calcStartDate(){
    // Determine the start date (= date of the last Monday)
    const day = this.today.getDay(); //.getDay outputs the day of the week in number (ie, Tue= 2)
    let startDate;
    if(day == 0){
      startDate = moment(this.today).subtract(6, 'day')
    }
    else{
      startDate = moment(this.today).subtract(day-1, 'day')
    }
      this.startDate = new Date(startDate);
  }

  calcEndDate(){
    // Determine the end date (= date of start date + 13 days)
    const endDate = moment(this.startDate).add(13 , 'day');
    this.endDate = new Date(endDate)
  }

  getDates(){
    // Generate an array of dates between start date and <= date
    const arr = []
    const dt = new Date(this.startDate);

    while (dt <= this.endDate) {
      arr.push(new Date(dt)) /*.toLocaleDateString("en-US", options));*/
      dt.setDate(dt.getDate() + 1);
    }
    this.dateRange = arr
  }

  getPastDates(){
    const arr2 = []
    this.dateRange.forEach((el, index) => {
      if (el < moment(this.today).subtract(1, 'hour')){
        arr2.push(index)
      }
    })
    this.pastDates = arr2
  }

  getTodayIndex(){
    const arr3 = []
    this.dateRange.forEach((el, index) => {
        if (moment(el).format("MMM Do YY") === moment(this.today).format("MMM Do YY")){
          arr3.push(index)
        }
    })
    this.todayIndex = arr3
  }

  getMonth(){
    let count = 0
    this.dateRange.forEach((el) => {
      if (el.getMonth() === this.startDate.getMonth()){
        count += 1
      }
    })

    if (count >= 7){
      this.month = this.startDate
    } else {
      this.month = this.endDate
    }
  }

  getSelectedDateID(click){
    const innerHTML  = click.target.id
    const id = /date-(\d+)/.exec(innerHTML)[1]
    return id
  }

  getSelectedDay(id){
    this.selectedDay = this.dateRange[id]
  }


  assignAgent(intervals){
    var format = 'hh:mm:ss'

    // var time = moment() gives you current time. no format required.
    var time = moment('20:29:00',format),
      beforeTime = moment('18:30:00', format),
      afterTime = moment('20:30:00', format);

    if (time.isBetween(beforeTime, afterTime)) {

      console.log('is between')

    } else {

      console.log('is not between')

    }
  }




}
