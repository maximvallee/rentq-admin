from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse



# Create your models here.
class User(AbstractUser):

    TZ_CHOICES = (
        (-10, 'Hawaii Standard Time (UTC -10)'),
        (-8, 'Alaska Daylight Time (UTC -8)'),
        (-7, 'Pacific Daylight Time (UTC -7)'),
        (-6, 'Mountain Daylight Time (UTC -6)'),
        (-5, 'Central Daylight Time (UTC -5)'),
        (-4, 'Eastern Daylight Time (UTC -4)'),
        (0, 'Universal Time Coordinated (UTC 0)'),
    )
    is_manager = models.BooleanField(default=False)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=30)
    phone = models.CharField(max_length=14)
    timezone = models.IntegerField(choices=TZ_CHOICES, default=0)

    def get_absolute_url(self):
        return reverse('home')
