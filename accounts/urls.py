from django.urls import path
from django.contrib.auth import views as auth_views
from accounts import views
from django.urls import reverse_lazy

app_name = 'accounts'

urlpatterns = [
    path('login/', views.user_login, name='user_login'),
    path('logout/', views.user_logout, name='logout'),
    path('signup/staff/', views.StaffSignUpView.as_view(), name='staff_signup'),
    path('signup/manager/', views.ManagerSignUpView.as_view(), name='manager_signup'),

    path('password_change/', auth_views.PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    path('password_reset/', auth_views.PasswordResetView.as_view(success_url = reverse_lazy('accounts:password_reset_done')), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(success_url = reverse_lazy('accounts:password_reset_complete')), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    path('user/<int:pk>/settings/time_parameters/', views.TimeParametersUpdateView.as_view(), name='time_parameters')
]
