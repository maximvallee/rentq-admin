from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
# Register your models here.

from accounts.forms import StaffSignUpForm
from accounts.models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'email']

admin.site.register(User, UserAdmin)
