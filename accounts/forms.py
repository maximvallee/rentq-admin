from django import forms
from django.forms import DateInput
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction

from crm.models import Staff, Manager
from accounts.models import User
from django.contrib.auth.forms import PasswordResetForm


class StaffSignUpForm(UserCreationForm):

    # phone = forms.CharField(required=True)
    # name = forms.CharField(required=True)
    # last_name = forms.CharField(required=True)

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('first_name','last_name','username','phone','email')


    def __init__(self, user, *args, **kwargs):
        # import pdb; pdb.set_trace()
        super(StaffSignUpForm, self).__init__(*args, **kwargs)
        self._user = Manager.objects.filter(user_id=user.id) #User (active) is an extra argument created in the Views and passed to the __init__
        self.fields['password1'].required = False
        self.fields['password2'].required = False
        self.fields['password1'].widget.attrs['autocomplete'] = 'off'
        self.fields['password2'].widget.attrs['autocomplete'] = 'off'



    @transaction.atomic
    def save(self, *args, **kwargs):
        #SAVE ADDITIONAL INFO (ie, is_staff, manager, random password)
        
        user = super().save(commit=False)
        user.is_staff = True
        user.password1 = self.cleaned_data.get("hello123!")
        user.save()
        self.new_user = user
        staff = Staff.objects.create(user=user)
        # import pdb; pdb.set_trace()
        staff.manager.add(self._user[0]) #Active user is only accessible through Views, so we must import it to __init__ as a self.variable in order to make it accessible to save() method
        staff.save()


        ##Send email requesting password
        def request_password(self):
           from django.http import HttpRequest
           # import pdb; pdb.set_trace()

           user = User.objects.get(id=self.new_user.id)
           form = PasswordResetForm({'email': user.email})

           if form.is_valid():
                request = HttpRequest()
                request.META['SERVER_NAME'] = 'www.mydomain.com'
                request.META['SERVER_PORT'] = '443'
                form.save(
                    request= request,
                    use_https=True,
                    from_email="RentQ <hello@rentq.io>",
                    subject_template_name="registration/password_request_subject.txt",
                    email_template_name='registration/password_request_email.html')
           return user
        request_password(self)
        return user




class ManagerSignUpForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username','email',)

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_manager = True
        user.save()
        manager = Manager.objects.create(user=user)
        manager.save()
        return user


class TimeParametersForm(forms.ModelForm):

    class Meta():
        model = User
        fields = ('timezone',)
