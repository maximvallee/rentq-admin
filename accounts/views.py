from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.shortcuts import redirect
from django.views.generic import CreateView, UpdateView
from django.contrib.auth.decorators import login_required

from accounts.forms import StaffSignUpForm, ManagerSignUpForm, TimeParametersForm
from accounts.models import User
from django.contrib.auth.forms import PasswordResetForm

# Create your views here.


class StaffSignUpView(CreateView):
    model = User
    form_class = StaffSignUpForm
    template_name = 'registration/staff_signup_form.html'
    # import pdb; pdb.set_trace()


    ## Send Active User ID to the form's __init__ method
    def get_form_kwargs(self):
        kwargs = super(StaffSignUpView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    ## Send context dict to the template
    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'staff'
        return super().get_context_data(**kwargs)

        def form_valid(self, form):
            user = form.save()
            login(self.request, user)


class ManagerSignUpView(CreateView):
    model = User
    form_class = ManagerSignUpForm
    template_name = 'registration/manager_signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'manager'
        return super().get_context_data(**kwargs)

        def form_valid(self, form):
            user = form.save()
            login(self.request, user)

class TimeParametersUpdateView(UpdateView):
    model = User
    form_class = TimeParametersForm
    template_name = 'registration/time_parameters_form.html'

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username,password=password)

        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(reverse('home'))
            else:
                return HttpResponse("Account not active")
        else:
            print("Someone tried to login and failed!")
            print("Username:{} and password:{}".format(username,password))
            return HttpResponse('Invalid login details supplied!')
    else:
        return render(request,'registration/login.html',{})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))
