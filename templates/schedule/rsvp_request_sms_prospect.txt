Hi {{PROSPECT_NAME}},

You have a viewing scheduled {{IS_TODAY}} at {{SHORT_TIME}} at {{SHORT_ADDRESS}}.

Kindly CONFIRM your attendance here: {{PROTOCOL}}://{{DOMAIN}}/calendar/status/{{PK_APPOINTMENT}}/

If you can no longer attend, please CANCEL your viewing here: {{PROTOCOL}}://{{DOMAIN}}/calendar/appointments/{{PK_APPOINTMENT}}/cancel/

RentQ