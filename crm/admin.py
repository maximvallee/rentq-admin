from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
# Register your models here.

from accounts.forms import StaffSignUpForm
from crm.models import User, Staff, Manager, Unit, Instruction, Prospect
from accounts.models import User


class UnitAdmin(admin.ModelAdmin):
    list_display = ['id', 'address']

class StaffAdmin(admin.ModelAdmin):
    list_display = ['user_id', 'user']

class InstructionAdmin(admin.ModelAdmin):
    list_display = ['name']

class ProspectAdmin(admin.ModelAdmin):
    list_display = ['id', 'first_name', 'last_name', 'email', 'phone', 'unit_size', 'lease_term', 'occupation']

admin.site.register(Manager)
admin.site.register(Staff, StaffAdmin)
admin.site.register(Unit, UnitAdmin)
admin.site.register(Instruction, InstructionAdmin)
admin.site.register(Prospect, ProspectAdmin)
