from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.shortcuts import redirect
from django.views.generic import CreateView, TemplateView, CreateView, UpdateView, DeleteView
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.db.models import Q
from django.template import RequestContext


from crm.forms import UnitForm, StaffForm, ManagerProfileForm, StaffProfileForm, InstructionForm
from crm.models import Unit, Staff, Manager, Prospect, Instruction
from accounts.models import User
from accounts.forms import StaffSignUpForm, ManagerSignUpForm

# Create your views here.



class IndexView(TemplateView):
    template_name = 'crm/index.html'

######## EXCEPTIONS ########

def handler404(request, exception, template_name="crm/404.html"):
    response = render(request,template_name)
    response.status_code = 404
    return response

def handler500(request, template_name="crm/500.html"):
    response = render(request,template_name)
    response.status_code = 500
    return response

######## UNITS ########

class UnitListView(TemplateView):
    template_name = 'crm/unit_list.html'

    def get_context_data(self, **kwargs):
        user = self.request.user
        if user.is_manager:
            kwargs['units'] = Unit.objects.filter(manager=user.id)
        else:
            kwargs['units'] = Unit.objects.filter(staff__user_id=user.id)
        return super().get_context_data(**kwargs)

class UnitCreateView(CreateView):
    template_name = 'crm/unit_form.html'
    form_class = UnitForm
    model = Unit

    ## Add a kwarg 'user' (active user) to the form's kwargs. This user (ie, manager)
    ## is used to filter and display only his agents in the MultiSelect
    def get_form_kwargs(self):
        kwargs = super(UnitCreateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    ## Set default value of manager to active user when creating new unit
    def form_valid(self, form):
        # import pdb; pdb.set_trace()
        user=self.request.user
        form.instance.manager = Manager.objects.get(user=user)
        return super(UnitCreateView, self).form_valid(form)

class UnitUpdateView(UpdateView):
    template_name = 'crm/unit_form.html'
    form_class = UnitForm
    model = Unit

    ## Add a kwarg 'user' (active user) to the form's kwargs. This user (ie, manager)
    ## is used to filter and display only his agents in the MultiSelect
    def get_form_kwargs(self):
        # import pdb; pdb.set_trace()
        kwargs = super(UnitUpdateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, **kwargs):
        kwargs['zoomEnabled'] = self.object.manager.zoom_meeting_room != ''
        return super().get_context_data(**kwargs)

class UnitDeleteView(DeleteView):
    template_name = 'crm/unit_confirm_delete.html'
    model = Unit
    success_url = reverse_lazy('crm:unit_list')

def clone_unit(request, pk):
    clone = Unit.objects.get(id=pk)
    clone.pk = None
    clone.save()
    # return render (request,'crm/unit_list.html')
    return redirect('/properties/')

def UnitCalendarView(request, pk):
    return render (request,'rentq/index.html')


######## STAFF ########

class StaffListView(TemplateView):
    template_name = 'crm/staff_list.html'

    def get_context_data(self, **kwargs):
        user = self.request.user.id
        kwargs['staff'] = Staff.objects.filter(manager=user)
        return super().get_context_data(**kwargs)


class StaffUpdateView(UpdateView):
    template_name = "registration/staff_update_form.html"
    model = Staff
    form_class = StaffForm

    def get_context_data(self, **kwargs):
        # import pdb; pdb.set_trace()
        user = self.get_object()
        kwargs['staff'] = Staff.objects.get(user_id=user)
        return super().get_context_data(**kwargs)

    ## Send Active User ID to the form's __init__ method
    def get_form_kwargs(self):
        kwargs = super(StaffUpdateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

class StaffDeleteView(DeleteView):
    template_name = 'crm/staff_confirm_delete.html'
    model = User
    success_url = reverse_lazy('crm:staff_list')

class StaffProfileView(UpdateView):
    template_name = "crm/staff_profile_form.html"
    model = Staff
    form_class = StaffProfileForm


######## PROSPECT ########

class ProspectListView(TemplateView):
    template_name = 'crm/prospect_list.html'

    def get_context_data(self, **kwargs):
        # import pdb; pdb.set_trace()
        user = self.request.user
        if user.is_manager:
            kwargs['prospects'] = Prospect.objects.filter(appointment__staff__manager__user_id=user.id).distinct()
        else:
            kwargs['prospects'] = Prospect.objects.filter(appointment__staff__user_id=user.id).distinct()
        return super().get_context_data(**kwargs)


class ProspectProfileView(TemplateView):
    template_name = 'crm/prospect_profile.html'

    def get_context_data(self, **kwargs):
        # import pdb; pdb.set_trace()
        prospect = self.kwargs['pk']
        user = self.request.user
        kwargs['prospect'] = Prospect.objects.get(id=prospect)
        if user.is_manager:
            kwargs['units_seen'] = Unit.objects.filter(Q(appointment__prospect_id=prospect) & Q(manager_id=user.id)).distinct()
        else:
            kwargs['units_seen'] = Unit.objects.filter(Q(appointment__prospect_id=prospect) & Q(manager_id=Manager.objects.filter(staff=user.id)[0].user_id)).distinct()
        return super().get_context_data(**kwargs)


######## SETTINGS ########

class SettingsListView(TemplateView):
    template_name = 'crm/settings.html'

class ManagerProfileView(UpdateView):
    template_name = "crm/manager_profile_form.html"
    model = Manager
    form_class = ManagerProfileForm

class InstructionsListView(TemplateView):
    template_name = 'crm/instructions.html'

    def get_context_data(self, **kwargs):
        kwargs['instructions'] = Instruction.objects.all()
        return super().get_context_data(**kwargs)

class InstructionView(UpdateView):
    template_name = "crm/instruction_form.html"
    model = Instruction
    form_class = InstructionForm

class InstructionDeleteView(DeleteView):
    template_name = 'crm/instruction_confirm_delete.html'
    model = Instruction
    success_url = reverse_lazy('crm:instructions_list')

class InstructionCreateView(CreateView):
    template_name = "crm/instruction_form.html"
    model = Instruction
    form_class = InstructionForm







######## AVAILABILITY ########

# class AvailabilityCreateView(CreateView):
#     template_name = 'crm/availability_form.html'
#     form_class = AvailabilityForm
#     model = Availability
#
# class AvailabilityUpdateView(UpdateView):
#     template_name = 'crm/availability_form.html'
#     form_class = AvailabilityForm
#     model = Availability
#     pk_url_kwarg = 'pk_alt'
#
#
# class AvailabilityDeleteView(DeleteView):
#     template_name = 'crm/availability_confirm_delete.html'
#     model = Availability
#     success_url = reverse_lazy('crm:staff_list')
#     pk_url_kwarg = 'pk_alt'
#
# class AvailabilityListView(TemplateView):
#     template_name = 'crm/availability_list.html'
#
#     def get_context_data(self, **kwargs):
#         # import pdb; pdb.set_trace()
#         user = self.request.user
#         kwargs['availabilities'] = Availability.objects.filter(staff__manager=Manager.objects.get(user=user)).filter(staff=self.kwargs['pk'])
#         return super().get_context_data(**kwargs)
#

############     FOR STAFF     #########
# class MyCalendarCreateView(CreateView):
#     template_name = 'crm/mycalendar_form.html'
#     form_class = AvailabilityForm
#     model = Availability
#
#     def get_context_data(self, **kwargs):
#
#         context = super().get_context_data(**kwargs)
#         d = self.request.GET
#         context['next_month'] = d
#
#         return context
#
#
# class MyCalendarUpdateView(UpdateView):
#     template_name = 'crm/mycalendar_form.html'
#     form_class = AvailabilityForm
#     model = Availability
#
# class MyCalendarListView(TemplateView):
#     # import pdb; pdb.set_trace()
#     template_name = 'crm/mycalendar_list.html'
#
#     def get_context_data(self, **kwargs):
#         # import pdb; pdb.set_trace()
#         user = self.request.user
#         kwargs['availabilities'] = Availability.objects.filter(staff=user.id)
#         return super().get_context_data(**kwargs)
#
# class MyCalendarDeleteView(DeleteView):
#     template_name = 'crm/mycalendar_confirm_delete.html'
#     model = Availability
#     success_url = reverse_lazy('crm:calendar_list')
