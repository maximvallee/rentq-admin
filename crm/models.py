from django.db import models
from django.urls import reverse
from accounts.models import User
from django.core.validators import MaxValueValidator, RegexValidator


# from django.utils.crypto import get_random_string


class Manager(models.Model):

    REMINDER_TIMER_CHOICES = (
        (12, '12 hours'),
        (24, '24 hours'),
        (36, '36 hours'),
        (48, '48 hours')
    )

    RSVP_TIMER_CHOICES = (
        (1.0, '60 minutes'),
        (1.25, '75 minutes'),
        (1.5, '90 minutes'),
        (1.75, '105 minutes'),
        (2.0, '120 minutes'),
    )

    INTERVAL_CHOICES = (
        (10, '10 minutes'),
        (15, '15 minutes'),
        (20, '20 minutes'),
        (30, '30 minutes'),
        (45, '45 minutes'),
        (60, '60 minutes'),
        (90, '90 minutes'),
        (120, '120 minutes')
    )

    QUEUE_SPREAD_CHOICES = (
        (15, '15 minutes'),
        (30, '30 minutes'),
        (45, '45 minutes'),
        (60, '60 minutes'),   
    )
    
    QUEUE_THRESHOLD_CHOICES = (
        (1, '1 hour'),
        (2, '2 hours'),
        (3, '3 hours'),
        (4, '4 hours'),
        (5, '5 hour'),
        (6, '6 hours'),
        (7, '7 hours'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    staff = models.ManyToManyField('Staff', related_name='+', blank=True)
    company = models.CharField(max_length=40)
    logo = models.ImageField(upload_to='logo_image', blank=True)
    reminder_timer = models.IntegerField(choices=REMINDER_TIMER_CHOICES, default=24)
    rsvp_timer = models.FloatField(choices=RSVP_TIMER_CHOICES, default=1.25)
    minimum_notice = models.PositiveIntegerField(default=36, validators=[MaxValueValidator(72)])
    interval = models.PositiveIntegerField(choices=INTERVAL_CHOICES, default=15)
    queue_spread = models.PositiveIntegerField(choices=QUEUE_SPREAD_CHOICES, default=15)
    queue_threshold = models.PositiveIntegerField(choices=QUEUE_THRESHOLD_CHOICES, default=2)
    zoom_meeting_room = models.URLField(blank=True)


    def __str__(self):
        return User.objects.get(id=self.user_id).username
    
    def get_absolute_url(self):
        return reverse('home')

class Unit(models.Model):

    APPOINTMENT_TYPE_CHOICES = (
        ('zoom', 'Zoom only'),
        ('in_person', 'In person only'),
        ('both', 'Both'),
    )

    address = models.CharField(max_length=50)
    unit_no = models.CharField(max_length=5, blank=True,)
    manager = models.ForeignKey(Manager, on_delete=models.CASCADE)
    staff = models.ManyToManyField('Staff', related_name='+', blank=True)
    appointment_type = models.CharField(max_length=40, choices=APPOINTMENT_TYPE_CHOICES, default='in_person')


    def __str__(self):
        return self.address.split(',')[0]

    def get_absolute_url(self):
        return reverse('crm:unit_list')


class Staff(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    manager = models.ManyToManyField(Manager, through=Manager.staff.through, related_name='+', blank=True)
    unit = models.ManyToManyField(Unit, through=Unit.staff.through, related_name='+', blank=True)
    profile_pic = models.ImageField(upload_to='profile_pic', blank=True)
    virtual_appointment_url = models.URLField(blank=True)
    virtual_appointment_password = models.CharField(blank=True, max_length=10, validators=[RegexValidator(r'^\d{1,10}$')])

    def __str__(self):
        return User.objects.get(id=self.user_id).username

    def get_absolute_url(self):
        return reverse('crm:staff_list')


class Prospect(models.Model):

    PRIV = 'Private bedroom'
    STUD = 'Studio'
    BDR1 = '1-bedroom unit'
    BDR2 = '2-bedroom unit'
    BDR3 = '3-bedroom unit'
    BDR4 = '4-bedroom unit'
    BDR5 = '5-bedroom unit'
    BDR6 = '+6-bedroom unit'

    UNIT_CHOICES = (
        (PRIV, 'Private bedroom'),
        (STUD, 'Studio'),
        (BDR1, '1-bedroom unit'),
        (BDR2, '2-bedroom unit'),
        (BDR3, '3-bedroom unit'),
        (BDR4, '4-bedroom unit'),
        (BDR5, '5-bedroom unit'),
        (BDR6, '+6-bedroom unit'),
    )

    ST = "Student"
    YP = "Young professional"
    MP = "Mature professional"
    RT = "Retiree"
    UP = "Unemployed"

    OCCUPATION_CHOICES = (
        (ST, 'Student'),
        (YP, 'Young professional'),
        (MP, 'Mature professional'),
        (RT, 'Retiree'),
        (UP, 'Unemployed'),
    )

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(max_length=40)
    phone = models.CharField(max_length=20)
    unit_size = models.CharField(max_length=15, choices=UNIT_CHOICES)
    lease_term = models.PositiveIntegerField(validators=[MaxValueValidator(12)])
    occupation = models.CharField(max_length=19, choices=OCCUPATION_CHOICES)

    def __str__(self):
        return self.first_name


class Instruction(models.Model):
    name = models.CharField(max_length=50)
    value = models.TextField(max_length=200)
    unit = models.ManyToManyField('Unit', blank=True)

    def get_absolute_url(self):
        return reverse('crm:instructions_list')



