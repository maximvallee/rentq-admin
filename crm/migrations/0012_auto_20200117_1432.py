# Generated by Django 3.0 on 2020-01-17 19:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0011_auto_20200117_0011'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prospect',
            name='unit_size',
            field=models.CharField(choices=[('Private bedroom', 'Bedroom'), ('Studio', 'Studio'), ('1-bedroom unit', '1-bedroom'), ('2-bedroom unit', '2-bedroom'), ('3-bedroom unit', '3-bedroom'), ('4-bedroom unit', '4-bedroom'), ('5-bedroom unit', '5-bedroom'), ('+6-bedroom unit', '+6-bedroom')], max_length=15),
        ),
    ]
