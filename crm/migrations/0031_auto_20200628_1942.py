# Generated by Django 3.0 on 2020-06-28 23:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0030_unit_virtual_enabled'),
    ]

    operations = [
        migrations.RenameField(
            model_name='unit',
            old_name='virtual_enabled',
            new_name='virtual_appointment',
        ),
    ]
