# Generated by Django 3.0 on 2020-05-13 03:46

from django.db import migrations
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0016_auto_20200425_1842'),
    ]

    operations = [
        migrations.AddField(
            model_name='prospect',
            name='unit_size_2',
            field=multiselectfield.db.fields.MultiSelectField(choices=[('item_key1', 'Private bedroom'), ('item_key2', 'Studio'), ('item_key3', '1-bedroom unit'), ('item_key4', '2-bedroom unit'), ('item_key5', '3-bedroom unit')], default='Studio', max_length=49),
            preserve_default=False,
        ),
    ]
