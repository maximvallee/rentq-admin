# Generated by Django 3.0 on 2020-01-16 05:43

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0009_auto_20200114_0252'),
    ]

    operations = [
        migrations.CreateModel(
            name='Prospect',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=30)),
                ('phone', models.CharField(max_length=20)),
                ('unit_size', models.CharField(choices=[('STUDIO', 'Studio'), ('1-BEDROOM', '1-bedroom'), ('2-BEDROOM', '2-bedroom'), ('3-BEDROOM', '3-bedroom'), ('4-BEDROOM', '4-bedroom'), ('5-BEDROOM', '5-bedroom'), ('+6-BEDROOM', '+6-bedroom')], max_length=10)),
                ('lease_term', models.PositiveIntegerField(validators=[django.core.validators.MaxValueValidator(12)])),
                ('occupation', models.CharField(choices=[('STUDENT', 'Student'), ('YOUNG PROFESSIONAL', 'Young professional'), ('MATURE PROFESSIONAL', 'Mature professional'), ('RETIREE', 'Retiree'), ('UNEMPLOYED', 'Unemployed')], max_length=19)),
            ],
        ),
    ]
