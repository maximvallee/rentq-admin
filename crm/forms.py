from django import forms
from django.forms import DateInput
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction

from crm.models import Staff, Manager, Unit, Instruction
from accounts.models import User
from django.contrib.auth.forms import PasswordResetForm
# from crm.views import request_password

# class AvailabilityForm(forms.ModelForm):
#
#     class Meta():
#         model = Availability
#         exclude = []
#         widgets = {
#           'start': DateInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
#           'end': DateInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
#         }
#
#     def __init__(self, *args, **kwargs):
#         super(AvailabilityForm, self).__init__(*args, **kwargs)
#         # import pdb; pdb.set_trace()
#         # input_formats to parse HTML5 datetime-local input to datetime field
#         self.fields['start'].input_formats = ('%Y-%m-%dT%H:%M',)
#         self.fields['end'].input_formats = ('%Y-%m-%dT%H:%M',)


class UnitForm(forms.ModelForm):
    address= forms.CharField(widget= forms.TextInput(attrs={'placeholder':'Enter the address of the property'}))
    unit_no= forms.CharField(required=False, widget= forms.TextInput(attrs={'placeholder':'Optional'}))

    class Meta():
        model = Unit
        fields = ('address', 'unit_no', 'staff', 'appointment_type')

    
    def __init__(self, user, *args, **kwargs):
        super(UnitForm, self).__init__(*args, **kwargs)
        # Only display staff associated to the the manager (ie, active user)
        self.fields['staff'].queryset = Staff.objects.filter(manager=user.id)
        # Only display appointment_type dropdown if a zoom_meeting_room (url) has been defined
        if user.manager.zoom_meeting_room == '':
            del self.fields['appointment_type']

class StaffForm(forms.ModelForm):
    class Meta():
        model = Staff
        exclude = ['manager']

    # Only display properties associated to the the manager (ie, active user)
    def __init__(self, user, *args, **kwargs):
        # import pdb; pdb.set_trace()
        super(StaffForm, self).__init__(*args, **kwargs)
        self.fields['unit'].queryset = Unit.objects.filter(manager=user.id)

class ManagerProfileForm(forms.ModelForm):
    class Meta():
        model = Manager
        fields = ('company', 'logo', 'reminder_timer', 'rsvp_timer', 'minimum_notice', 'interval', 'queue_spread', 'queue_threshold', 'zoom_meeting_room')
    
    # If the Zoom URL is blank, all units' appointment_type become in_person by default
    def save(self, commit=True):
            m = super(ManagerProfileForm, self).save(commit=False)
            if m.zoom_meeting_room == '':
                Unit.objects.filter(manager=m).update(appointment_type='in_person') 
            if commit:
                m.save()
            return m


class StaffProfileForm(forms.ModelForm):
    class Meta():
        model = Staff
        fields = ['profile_pic']

class InstructionForm(forms.ModelForm):
    class Meta():
        model = Instruction
        fields = ['name', 'value', 'unit']