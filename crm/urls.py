from django.urls import path
from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy

from crm import views

app_name = 'crm'

urlpatterns = [
    path('properties/', views.UnitListView.as_view(), name='unit_list'),
    path('properties/new/', views.UnitCreateView.as_view(), name='unit_new'),
    path('properties/<int:pk>/edit', views.UnitUpdateView.as_view(), name='unit_edit'),
    path('properties/<int:pk>/delete', views.UnitDeleteView.as_view(), name='unit_delete'),
    path('properties/<int:pk>/clone', views.clone_unit, name='unit_clone'),
    path('unit/<int:pk>/', views.UnitCalendarView, name='unit_calendar'),
    # path('properties/<int:pk>/calendar', views.PropertyCalendarView.as_view(), name='property_calendar'),

    path('staff/', views.StaffListView.as_view(), name='staff_list'),
    path('staff/<int:pk>/edit', views.StaffUpdateView.as_view(), name='staff_edit'),
    path('staff/<int:pk>/delete', views.StaffDeleteView.as_view(), name='staff_delete'),
    path('staff/<int:pk>/profile', views.StaffProfileView.as_view(), name="staff_profile"),

    path('settings/', views.SettingsListView.as_view(), name="settings_list"),
    path('settings/manager/<int:pk>/profile', views.ManagerProfileView.as_view(), name="manager_profile"),
    path('settings/instructions', views.InstructionsListView.as_view(), name="instructions_list"),
    path('settings/instructions/new', views.InstructionCreateView.as_view(), name="instruction_new"),
    path('settings/instructions/<int:pk>/edit', views.InstructionView.as_view(), name="instruction_edit"),
    path('settings/instructions/<int:pk>/delete', views.InstructionDeleteView.as_view(), name="instruction_delete"),

    path('prospects/<int:pk>/profile', views.ProspectProfileView.as_view(), name='prospect_profile'),
    path('prospects/', views.ProspectListView.as_view(), name='prospect_list')

]
